
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Usuario</title>
        <link rel="stylesheet" type="text/css" href="EstiloRegistroUsuario.css">
        <script src="js/validarRegistroUsuario.js"></script>
         <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    
    <body>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
        
        <div id="cuadroRegistroUsuario">
            <h1 id="titulo_RegistroUsuario">Registro de Usuario</h1>
            
            <form id="formulario_RegistroUsuario" method="post" action="./insertarusuario" onsubmit="return validarRegistro()">
                Nombre: <br>
                <input id="nombre" type="text" name="nombre"> <br><br>
                
                Apellidos: <br>
                <input id="apellidos" type="text" name="apellidos"> <br><br>

                Cedula: <br>
                <input id="cedula" type="text" name="cedula"> <br><br>
                
                Contraseña: <br>
                <input id="contrasena" type="password" class="entrada" name="ncontrasena"/> <br><br>
                
                Telefono: <br>
                <input id="telefono" type="tel" name="telefono" placeholder="71174852"> <br><br>

                Correo: <br>
                <input id="correo" type="email" name="correo" placeholder="nombre@gmail.com"> <br><br>

                Direccion: <br>
                <input id="direccion" type="text" name="direccion"> <br><br>

                Tipo Usuario: <br>
                <select id="tipoUsuario" name="tipoUsuario">
                    <option value="1"> Arrendador</option>
                    <option value="2"> Inquilino</option>
                </select> <br><br>

                <input type="submit" value="Registrar" class="boton">
            </form>
            
            <!--<form action="./cargarIndex?opcion=1" method="post">
                <input type="submit" value="Atras"/>
            </form> -->           
        </div>
        
     <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>   
    </body>
</html>
