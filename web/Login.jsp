<%-- 
    Document   : Login
    Created on : 09/03/2020, 07:04:23 PM
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <link rel="stylesheet" type="text/css" href="EstiloLogin.css">
        <script src="js/validarlogin.js"></script>
    </head>
    
    <body>
      
        <input id="err" type="text" name="ncedula" style="visibility: hidden"  value="${err}">
       
        
        <script>
            ValidarSesion();
        </script> 
        
        <header class="header">
            <div class="container logo-nav-container">
                <a href="./cargarIndex?opcion=1" class="logo"> <img src="logo.png" width="110" height="80" alt="">  </a>
                <nav class="navegacion">
                    <ul class="itemprincipal">
                        <li><a href="./cargarIndex?opcion=1">Inicio</a></li>
                        <li><a href="./Registro_Usuario.jsp">Registrarme</a></li>
                        <li><a href="#">Acerca de</a></li>
                        
                    </ul>
                </nav>
            </div>
        </header>
        
        <div id="cuadro">
            <form action="./iniciarsecion" method="post" onsubmit="return validarSesion()">
                <p id="titulo">Iniciar Sesion</p>
                <hr>
                <br><br>
                <label id="subtitulo1"> Usuario </label>
                <br>
                <input type="text" class="entrada" name="nusuario" id="nusuario" placeholder="Cedula"/>
                <br>
                <label id="subtitulo2"> Contraseña </label>
                <br>
                <input type="password" class="entrada" name="ncontrasena" id="ncontrasena" placeholder="Contraseña"/>
                <br><br>
                <input type="submit" value="Iniciar Sesion" id="btnIniciarSesion"/>
                                
            </form>
            <br>
            
            <div id="botonera">
                <form action="./Registro_Usuario.jsp" method="post">
                    <input type="submit" value="Crear Cuenta"/>
                </form>
                
                <form action="./cargarIndex?opcion=1" method="post">
                    <input type="submit" value="Atras"/>
                </form>
            </div>
            
            <form action="./EnviarCorreo" method="post" onsubmit="return validarRecuperacion()">
                <br>
                <h3>Digite la cédula para recuperar la contraseña</h3>
                <input type="text"  class="entrada" id="ncedula" name="ncedula" placeholder="Cedula"/>
                 <br>
                 <input type="submit" value="Enviar recuperacion al correo"/> 
            </form>
            
            <p id="marca">InfoRenta</p>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 
    </body>
</html>
