<%-- 
    Document   : RecuperarCuenta
    Created on : 11/04/2020, 05:57:26 PM
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RecuperarCuenta</title>
        <script src="js/recuperarContraseña.js"></script>
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
        <script src="js/validarRecuperacionContrasena.js"></script>
    </head>
    <body>
         
         <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script>    
        
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        <form id="recuperacion" action="./RecuperarContrasena" method="get" onsubmit="return validarDatos()">
                <label id="subtitulo2"> Clave de seguridad </label>
                <br><br>
                <input type="text" class="entrada" id="nclave" name="nclave"/>
                <br><br>
                <label id="subtitulo2"> Nueva contraseña</label>
                <br><br>
                <input type="password" class="entrada" id="ncontrasena" name="ncontrasena"/>
                
                 <label id="subtitulo2"> Confirmar contraseña</label>
                <br><br>
                <input type="password" class="entrada" id="ncontrasena2" name="ncontrasena2"/>
                
                 <input type="submit" value="Cambiar Contraseña" id="btnCambiarContrasena"/> 
        </form>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 
    </body>
</html>
