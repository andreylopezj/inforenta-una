<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <%
             int numeroContrato = Integer.parseInt(request.getParameter("idContrato"));
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Modificar Contrato</title>
       <link rel="stylesheet" type="text/css" href="EstiloRegistrarContrato.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    <body>
 
 <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script>         
          <br>
          <br>
          <br>
          <br>
<div class="container">
<h1>Modificar Contrato</h1>
<hr>
 <form method="post" action="./modificarcontrato?ced=${usuario.cedula}">
    <div class="well">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                   
                  <div class="col-sm-8">                  
                  </div>
                </div>
            </div>
                      
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                   <label for="tituloArrendador"><br><b>Parte arrendador (Propietario/a)</b></label>
                  </div>
                </div>
            </div>
            
            
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                   
                   Id contrato: <input type="text" name="numeroContrato" value="<%=numeroContrato%>"><br>
                  </div>
                </div>
            </div>
                  
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                  Telélefono: <input type="tel" name="mtelefono" placeholder="71174852" pattern="[0-9]{8,8}$" class="required number" oninvalid="alert('Ingresa nuevamente el número de teléfono!');" required="">
                  </div>
                </div>
            </div>
           
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8">
                      <label for="tituloValidez"><br><b>Precio alquiler y validez del contrato</b></label><br>
                Precio mensual: <input type="text" name="mmonto" class="required number" oninvalid="alert('Ingresa nuevamente el monto!');" required=""><br>
                Plazo de validez del contrato: <input type="text" name="mvalidezContrato" placeholder="3 años" required><br>
                Número de cuenta: <input type="text" name="mnumeroCuenta" size="40" required>
                  </div>
                </div>
            </div>
            
           
            
            
   
            <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group row">
                <div class="col-sm-8">
                <br><br><br><br><br><input id="Registrar" type="submit" value="Modificar" class="btn btn-danger btn-block btn-lg" tabindex="7">
                </div>
            </div>       
            
</form>
         
        
        </div>
            
    </body>
</html>
