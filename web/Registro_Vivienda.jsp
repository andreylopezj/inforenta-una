<%-- 
    Document   : Registro_Vivienda
    Created on : 06/03/2020, 04:58:24 PM
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Vivienda</title>
        <link rel="stylesheet" type="text/css" href="EstiloRegistroVivienda.css">
        <script src="js/validarRegistroVivienda.js"></script>
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    
    
    <body>
        
        <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
        
        <div class="cuadroRegistroVivienda">
            
            <label id="titulo_Vivienda">Registro de Viviendas</label><br>
        
            <form action="./insertarvivienda" id="formulario_Vivienda" method="POST" enctype="multipart/form-data" onsubmit="return validar()">
                <div class="cuadro" id="datosVivienda">
                    
                    <label>Direccion Vivienda:</label><br>
                    <input id="direccionVivienda" type="text" name="direccionVivienda"> <br><br>

                    <label>NISE Agua:</label><br>
                    <input id="niseAgua" type="text" name="niseAgua"> <br><br>

                    <label>NISE Luz:</label><br>
                    <input id="niseLuz" type="text" name="niseLuz"> <br><br>
                    
                    <label>Precio:</label><br>
                    <input id="precio" type="text" name="precio"> <br><br>
                    
                    <label>Imagenes:</label><br>
                    <label id="notaimagen">**Debe ingresar 5 imagenes(jpg-png)</label><br>
                    <input type="file" name="file[]" id="archivos" multiple>               
                   
                </div>
                
               
                <div class="cuadro" id="descripcionVivienda">   
                    
                    <label id="titulo2"> Descripcion de la vivienda</label><br>
                    
                    <label for="">Internet</label>
                    <label id="radio1">
                        <input id="internet1" type="radio" name="internet" value="1">Si
                        <input id="internet2" type="radio" name="internet" value="0">No
                    </label>
                    <br>
                    
                    <label for="">Niños</label>
                    <label id="radio2">
                        <input id="ninos1" type="radio" name="ninos" value="1">Si
                        <input id="ninos2" type="radio" name="ninos" value="0">No
                    </label>                    
                    <br>
                    
                    <label for="">Cable</label>
                    <label id="radio3">
                        <input id="cable1" type="radio" name="cable" value="1">Si
                        <input id="cable2" type="radio" name="cable" value="0">No
                    </label>                     
                    <br>
                    
                    <label for="">Mascotas</label>
                    <label id="radio4">
                        <input id="mascotas1" type="radio" name="mascotas" value="1">Si
                        <input id="mascotas2" type="radio" name="mascotas" value="0">No
                    </label>                    
                    <br>
                    
                    <label for="">Garage</label>
                    <label id="radio5">
                        <input id="garage1" type="radio" name="garage" value="1">Si
                        <input id="garage2" type="radio" name="garage" value="0">No
                    </label>                    
                    <br>
                    
                    <label for="">Amueblado</label>
                    <label id="radio6">
                        <input id="amueblado1" type="radio" name="amueblado" value="1">Si
                        <input id="amueblado2" type="radio" name="amueblado" value="0">No
                    </label>                     
                    <br>
                    
                    <label for="">Zona Verde</label>
                    <label id="radio7">
                        <input id="zonaverde1" type="radio" name="zonaverde" value="1">Si
                        <input id="zonaverde2" type="radio" name="zonaverde" value="0">No
                    </label>                     
                    <br>
                    
                    <label for="">Cuarto Pila</label>
                    <label id="radio8">
                        <input id="cuartopila1" type="radio" name="cuartopila" value="1">Si
                        <input id="cuartopila2" type="radio" name="cuartopila" value="0">No
                    </label>                     
                    <br>
                    
                    <label for="">Sistema Seguridad</label>
                    <label id="radio9">
                        <input id="seguridad1" type="radio" name="seguridad" value="1">Si
                         <input id="seguridad2" type="radio" name="seguridad" value="0">No
                    </label>                     
                    <br>
                    
                    <label for="">Visitas</label>
                    <label id="radio10">
                        <input id="visitas1" type="radio" name="visitas" value="1">Si
                        <input id="visitas2" type="radio" name="visitas" value="0">No
                    </label>                     
                    <br><br>
                    
                    <div id="camposdescripcion">
                        <label for="">Cantidad Cuartos</label>
                        <input id="cantCuartos" type="text" name="cantCuartos">
                        <br><br>
                        <label>Tipo Vivienda</label>
                        <input id="tipoVivienda" type="text" name="tipoVivienda">
                        <br><br>
                        <label for="">Cantidad Baños</label>
                        <input id="cantBanos" type="text" name="cantBanos">
                        <br><br>
                        <label for="">Dimensiones</label>
                        <input id="dimensiones" type="text" name="dimensiones">
                    </div>                    
                </div>
            
                    <input type="hidden" name="idCrud" value="1"/>
                    <input id="bnRegistro" type="submit" value="Registrar " class="boton"> <br>    
            </form>
            
            <!--<form  action ="./menuPrincipal?tipoUsuario=${usuario.tipoUsuario}" method="post">
                    <input type="hidden" name="tipoUsuario" value="${usuario.tipoUsuario}"/>
                    <input type="submit" value="Atras" id="atras"/>
                </form>-->
            <br>            
             
        </div> 
        
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>        
    </body>
</html>
