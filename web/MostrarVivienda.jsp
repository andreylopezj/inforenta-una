
<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cr.ac.una.inforenta.servlet.MostrarViviendaServlet"%>
<%@page import="cr.ac.una.inforenta.negocio.LogicaVivienda"%>

<!DOCTYPE html>

<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrar Vivienda</title>
        <link rel="stylesheet" type="text/css" href="EstiloMostrarVivienda.css">
        <script src="js/confirmarBorrado.js"></script>
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    
    <body>
        
        <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
          <br><br><br>
        <div id="cuadroMostrar" style="overflow: scroll;">
            
            <h1 id="titulo">Listas de Viviendas</h1>
            
            <table id="tabla" border="1">
                <th> Direccion</th>                
                <th> Nice Agua</th>
                <th> Nice Luz</th>
                <th> Precio</th>
                <th> Imagen1</th>
                <th> Imagen2</th>
                <th> Imagen3</th>
                <th> Imagen4</th>
                <th> Imagen5</th>
                
                <th> Internet </th>
                <th> Cable </th>
                <th> Mascotas </th>
                <th> Garage </th>
                <th> Zona Verde </th>
                <th> Cuarto Pila </th>
                <th> Dimensiones </th>
                <th> Niños </th>
                <th> Cantidad Cuartos </th>
                <th> Tipo Vivienda </th>
                <th> Amueblado </th>
                <th> Cantidad Baños </th>
                <th> Seguridad </th>
                <th> Visitas </th>
                <th> Acciones </th>
                
                
                <%
                    LogicaVivienda lvivienda;
                    lvivienda = new LogicaVivienda();
                    String lista = lvivienda.vivienda_descripcion();
                    String lista2[]= lista.split("#");

                    for (int i = 0; i < lista2.length; i++) {						
                        String lista3[] = lista2[i].split("-");
                %>
                
                    <tr>           
                        <td>
                            <label> <%=lista3[1]%></label>                                
                        </td>
                        <td>
                            <label> <%=lista3[2]%> </label>                                
                        </td>
                        <td>
                            <label> <%=lista3[3]%> </label>                                
                        </td>
                        <td>
                            <label> <%=lista3[4]%> </label>                          
                        </td>                        
                        <td>
                            <img src="<%=lista3[5]%>" width="120" height="80">
                        </td>
                        <td>
                            <img src="<%=lista3[6]%>" width="120" height="80">
                        </td>
                        <td>
                            <img src="<%=lista3[7]%>" width="120" height="80">
                        </td>
                        <td>
                            <img src="<%=lista3[8]%>" width="120" height="80">             
                        </td>
                        <td>
                            <img src="<%=lista3[9]%>" width="120" height="80">             
                        </td>
                                                
                        <!-- Descripcion -->
                        
                        <td>
                            <label> <%=lista3[10]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[11]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[12]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[13]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[14]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[15]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[16]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[17]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[18]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[19]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[20]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[21]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[22]%> </label>                          
                        </td>
                        <td>
                            <label> <%=lista3[23]%> </label>                          
                        </td>
                        
                        <td>
                            <!-- Actualizar -->
                            <form method="post" action="ModificarVivienda.jsp">
                                <input type="hidden" name="idVivienda" value="<%=lista3[0]%>"/>
                                <input id="boton" type="submit" value="Actualizar"/>
                            </form>
                                <br>
                            <!-- Eliminar -->
                            <form method="post" action="./EliminarVivienda" onsubmit="return confirmar()">                                
                                <input type="hidden" name="indiceEliminar" value="2"/>
                                <input type="hidden" name="idVivienda" value="<%=lista3[0]%>"/>
                                <input id="boton" type="submit" value="Eliminar"/>
                            </form>
                        </td>

                    </tr>
                
                    <%
                        }
                    %>
            </table>
            
            <!--<form  action ="./menuPrincipal?tipoUsuario=${usuario.tipoUsuario}" method="post">
                    <input type="hidden" name="tipoUsuario" value="${usuario.tipoUsuario}"/>
                    <input type="submit" value="Atras" id="atras"/>
                </form>-->
            
        </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 
    </body>
</html>
