<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<!DOCTYPE html>
<html>
    <head>
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Contrato de alquiler</title>
       <link rel="stylesheet" type="text/css" href="EstiloRegistrarContrato.css">
       <script src="js/validarRegistroContrato.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><!-- Esta seria la otra forma-->
        <script>
            $("#Registrar").click(function(){
                
                window.alert("Se inserto correctamente");
            });
            
        </script>
        
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    <body>
        
        <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 

          <br>
          <br>
          <br>
          <br>
<div class="container">
<h1>Contrato de alquiler</h1>
<hr>
 <form method="get" action="./registrarcontrato" onsubmit="return validarContrato()">
    <div class="well">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                    <label for="fecha" class="col-form-label col-sm-1">Fecha</label>
                  <div class="col-sm-8">
                  <input type="text" name="fecha" id="fecha" >
                  
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                   . 
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                   Id Vivienda <input type="text" id="idVivienda" name="idVivienda">
                  </div>
                </div>
            </div>
            
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                   <label for="tituloArrendador"><br><b>Parte arrendador (Propietario/a)</b></label>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                   <label for="tituloInquilino"><b>Datos Inquilino</b></label>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                  Número de cédula: <input type="text" id="cedula" name="cedula" value="${usuario.cedula}" readonly=»readonly»>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
               
                <select name="datos_inquilino">
                <c:forEach items="${listaInquilino}" var="lista">
                    <tr>
                        <option value="${lista.cedula}-${lista.nombre} ${lista.apellidos} ">${lista.cedula}-${lista.nombre} ${lista.apellidos}
                    </tr>
                </c:forEach>
            </select>
                  </div>
                </div>
            </div>
            
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                      Nombre completo:  <input type="text" id="nombre" name="nombre" value="${usuario.nombre}${usuario.apellidos}" readonly=»readonly»>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                
                <label for="tituloValidez"><br><b>Precio alquiler y validez del contrato</b></label><br>
                Precio mensual: <input type="text"  id="monto" name="monto" ><br>
                Plazo de validez del contrato: <input type="text" id="validezContrato" name="validezContrato" placeholder="3 años"><br>
                Número de cuenta: <input type="text"  id="numeroCuenta" name="numeroCuenta" size="40" >
                  </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                  <div class="col-sm-8"> 
                  Telélefono: <input type="tel" id="telefono" name="telefono" placeholder="71174852">
                  </div>
                </div>
            </div>
            
   
            <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group row">
                <div class="col-sm-8">
                <br><input id="Registrar" type="submit" value="Registrar" class="btn btn-danger btn-block btn-lg" tabindex="7">
                </div>
            </div>
        
            
         </form>
            
           
        </div>
         <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>    
    </body>
</html>
