
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cr.ac.una.inforenta.servlet.MostrarViviendaServlet"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Descripción</title>
        <link rel="stylesheet" type="text/css" href="EstiloMostrarDescripcion.css">
    </head>
    
    <body>
        
        <div id="tablaDescripcion">
            <h1 id="titulo">Descripción</h1>
            
            <label> Ociones (0 = No) - (1 = Si)</label>  <br>
            
            <table id="tabla" border="1">
                <th> Código Vivienda </th>
                <th> Internet</th>                
                <th> Cable</th>
                <th> Mascotas</th>
                <th> Garage</th>
                <th> Zona Verde</th>
                <th> Cuarto Pila</th>
                <th> Dimensiones</th>
                <th> Niños</th>
                <th> Cuartos</th>
                <th> Tipo Vivienda</th>
                <th> Amueblado</th>
                <th> Baños</th>
                <th> Sistema Seguridad</th>
                <th> Visitas</th>
                
                <c:forEach items="${listaDescripcion}" var="tempDes">
                    <tr>           
                        <td>
                            <label> ${tempDes.idVivienda}</label>                                
                        </td>
                        <td>
                            <label> ${tempDes.internet}</label>                                
                        </td>
                        <td>
                            <label> ${tempDes.cable}</label>                                
                        </td>
                        <td>
                            <label> ${tempDes.mascotas}</label>                          
                        </td>
                        <td>
                            <label> ${tempDes.garage}</label>                                
                        </td>
                        <td>
                            <label> ${tempDes.zonaVerde}</label>   
                        </td>
                        <td>
                            <label> ${tempDes.cuartoPila}</label>   
                        </td>
                        <td>
                            <label> ${tempDes.dimensiones}</label>   
                        </td>
                        <td>
                            <label> ${tempDes.ninos}</label> 
                        </td>
                        <td>
                            <label> ${tempDes.cantCuartos}</label>             
                        </td>
                        <td>
                            <label> ${tempDes.tipoVivienda}</label> 
                        </td>
                        <td>
                            <label> ${tempDes.amueblado}</label> 
                        </td> 
                        <td>
                            <label> ${tempDes.cantBanos}</label> 
                        </td> 
                        <td>
                            <label> ${tempDes.sistemaSeguridad}</label> 
                        </td>
                        <td>
                            <label> ${tempDes.visitas}</label> 
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <form method="post" action="./mostrarvivienda">                                
                <input type="hidden" name="indicador" value="1"/>
                <input id="boton" type="submit" value="Atras"/>
            </form>
            <a href="index.html" > Inicio</a>
            
        </div>
        
    </body>
</html>
