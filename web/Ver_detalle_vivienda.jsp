
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cr.ac.una.inforenta.servlet.MostrarViviendaServlet"%>
<%@page import="cr.ac.una.inforenta.negocio.LogicaVivienda"%>

<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link rel="stylesheet" type="text/css" href="EstiloDetalleVivienda.css">
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
        <title>Detalle Vivienda</title>
    </head>
    
    <body>
        
        <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
        
                
        <div class="contenedor">
            <%
                int id = Integer.parseInt(request.getParameter("id"));
                
                LogicaVivienda lvivienda;
                lvivienda = new LogicaVivienda();
                String lista = lvivienda.ver_detalleVivienda(id);
                String lista2[]= lista.split("#");

                for (int i = 0; i < lista2.length; i++) {						
                    String lista3[] = lista2[i].split("-");
            %>
            
            
            
            <!--------------------------------------------------------->
            <div id="cuadrofotos">
                <label id="direccion"><%=lista3[1]%></label> 
                
                <ul class="slider">
                    <li id="slide1"><img src="<%=lista3[5]%>" width="400" height="350"></li>
                    <li id="slide2"><img src="<%=lista3[6]%>" width="400" height="350"></li>
                    <li id="slide3"><img src="<%=lista3[7]%>" width="400" height="350"></li>
                    <li id="slide4"><img src="<%=lista3[8]%>" width="400" height="350"></li>
                    <li id="slide5"><img src="<%=lista3[9]%>" width="400" height="350"></li>
                </ul>
                <br><br><br><br><br><br><br><br><br>
                <nav>
                    <ul class="menu">
                        <li><a href="#slide1">Imagen 1</a></li>
                        <li><a href="#slide2">Imagen 2</a></li>
                        <li><a href="#slide3">Imagen 3</a></li>
                        <li><a href="#slide4">Imagen 4</a></li>
                        <li><a href="#slide5">Imagen 5</a></li>
                    </ul>
                </nav>
            </div>
            
            <!--------------------------------------------------------->
            <div id="infocasa">
                <label id="titulovievienda">Datos de la Vivienda</label><br>
                <hr color="black">
                <label>Nice agua: <%=lista3[2]%> </label><br><br>
                <label>Nice Luz: <%=lista3[3]%></label><br><br>
                <label id="tituloprecio">Precio: <%=lista3[4]%></label><br><br>
                
                <form action="./comboboxinquilino" id="formBoton" method="post" >
                    <input type="hidden" name="idVivienda" value="<%=lista3[0]%>"/> 
                    <input id="boton" type="submit" value="Realizar Contrato"/>
                </form>
                
            </div>
                
                <div id="infodes">
                    
                    <label id="titulodescripcion">Descripcion de la vivienda</label>
                    <hr color="black">
                    
                    <label id="columna1">Internet: <%=lista3[10]%> </label>                          
                    <br>
                    <label id="columna1">Cable: <%=lista3[11]%> </label>                          
                    <br>    
                    <label id="columna1">Mascotas: <%=lista3[12]%> </label>                          
                    <br>    
                    <label id="columna1">Garage: <%=lista3[13]%> </label>                          
                    <br>    
                    <label id="columna1">Zona Verde: <%=lista3[14]%> </label>                          
                    <br>    
                    <label id="columna1">Cuarto Pila: <%=lista3[15]%> </label>                          
                    <br>    
                    <label id="columna1">Dimencion: <%=lista3[16]%> </label>                          
                    <br>   
                    <label id="columna2">Niños: <%=lista3[17]%> </label>                          
                    <br>    
                    <label id="columna2">Cantidad Cuartos:  <%=lista3[18]%> </label>                          
                    <br>    
                    <label id="columna2">Tipo Vivienda:  <%=lista3[19]%> </label>                          
                    <br>    
                    <label id="columna2">Amueblado:  <%=lista3[20]%> </label>                          
                    <br>    
                    <label id="columna2">Cantidad Baños: <%=lista3[21]%> </label>                          
                    <br>   
                    <label id="columna2">Sistema Seguridad: <%=lista3[22]%> </label>                          
                    <br>    
                    <label id="columna2">Visitas: <%=lista3[23]%> </label>                          
                        
                </div>
            
            <%
                }
            %>
        </div>
        
    </body>
    
</html>
