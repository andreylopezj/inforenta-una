<%-- 
    Document   : MenuArrendador
    Created on : 05/04/2020, 03:17:53 PM
    Author     : andre
--%>

<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu Arrendador</title>
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    <body>
        
          <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
          
           <main class="main">
            
            <form action="./filtroVivienda" method="post">
                <div class="filtro caja">
                    <label id="textoFiltro">Filtro viviendas</label><hr color="black">

                    <label id="">Precio Vivienda</label><br>
                    <input id="precioVivienda " type="text" name="precioVivienda" placeholder="3000000"> <br><br>

                    <label id="">Cantidad de Cuartos</label><br>
                    <INPUT TYPE="NUMBER" MIN="0" MAX="10" STEP="2" VALUE="0" SIZE="6" name="cantCuartos">
                    <br><br>
                    
                    <input id="bnFiltro " type="submit" value="Buscar" class="boton"> <br>
                </div>
            </form>
            
            
            <div class="cajalista">
                
                <!--------------------------------------------------------------->
                                
                    <table border="0" align="center" width="950">
                        <% int jump = 0; %>
                        <c:forEach items="${listaviviendas}" var="vivienda">                            
                            
                            <th>
                                <form action="Ver_detalle_vivienda.jsp?id=${vivienda.idVivienda}" method="post">
                                    <input type="hidden" name="idVivienda" value="${vivienda.idVivienda}"/>
                                    <input type=image src="${vivienda.imagen1}" width="140" height="140">
                                </form>

                                <p>
                                    direccion: ${vivienda.direccion}<br>
                                    precio: ${vivienda.precio}
                                </p>
                            </th>
                            <%
                                jump++;
                                if(jump==3){
                            %>                    
                            <tr>                    
                            <%
                                jump=0;
                                }
                            %>
                
                        </c:forEach>
                    </table>
            </div>    
        </main>
                        
        <!--    PIE DE PAGINA    -->
        <footer class="footer">
            <div id="piepagina">
                <p>Desarrolladores web: Adrian Bonilla, Jacqueline Batista, Yoselyn Sánchez, Andrey Lopez</p>                
                <label id="texto1">© INFORENTA</label>
                <label id="texto2"> Aviso legal | Politicas de Privacidad </label>
            </div>
        </footer>
        
    </body>
    
</html>
