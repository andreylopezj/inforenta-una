<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrar Usuario Individual</title>
        <link rel="stylesheet" type="text/css" href="EstiloMostrarUsuarioIndividual.css">
    </head>
    
    <body>
                
        <div id="cuadroMostraUsuarioIndividual">
            <h1 id="titulo_MostraUsuarioIndividual">Mostrar Usuario Individual</h1>
            
            <%--placeholder="7117-4852"--%>
            <%--" method="post" action="./verusuario">--%>
            <form id="formulario_MostraUsuarioIndividual" action="index.html">
                Cédula del Usuario<input type="text" name="cedula" required/>
                <input type="submit" value="Mostrar" class="boton"><br/><br/><br/>
            

            <table border="3">
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Telefono</th>
                <th>Direccion</th>
                <th>Correo</th>

                <%--<c:set value="usuario" var="user"/>--%>
                <tr>
                     <td> <c:out value="${usuario.cedula}"/> </td>
                    <td> <c:out value="${usuario.nombre}"/> </td>
                    <td> <c:out value="${usuario.apellidos}"/> </td>
                    <td> <c:out value="${usuario.telefono}"/> </td>
                    <td> <c:out value="${usuario.direccion}"/> </td>
                    <td> <c:out value="${usuario.correo}"/> </td>
                </tr>
            </table>
            </form>
                
        </div>
        
    </body>
</html>