<%-- 
    Document   : Perfil_Usuario
    Created on : 02/06/2020, 06:24:01 PM
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Perfil</title>
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <link rel="stylesheet" type="text/css" href="EstiloModificarUsuario.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
        <script src="js/validarPerfilUsuario.js"></script>
    </head>
    <body>
         
       <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
          <br><br>
           <div id="cuadroModificarUsuario">
            <h1 id="titulo_ModificarUsuario">Modificar Usuario</h1>
            
            <form id="formulario_ModificarUsuario" method="post" action="./modificarusuario" onsubmit="return validarRegistro()">
                Cédula del Usuario a modificar <br> 
                <input type="text" id="cedula" name="cedula" value="${usuario.cedula}" readonly=»readonly»>
                <br><br>
                
                Nombre: <br>
                <input type="text" id="nombre" name="nombre" value="${usuario.nombre}">
                <br><br>
                
                Apellidos: <br>
                <input type="text" id="apellidos" name="apellidos" value="${usuario.apellidos}">
                <br><br>
              
                Telefono: <br>
                <input type="text" id="telefono" name="telefono" value="${usuario.telefono}">
                <br><br>
                
                Direccion: <br>
                <input type="text" id="direccion" name="direccion" value="${usuario.direccion}"> <br><br>
                
                Correo: <br>
                <input type="text" id="correo" name="correo" value="${usuario.correo}"> <br><br>

             
                <input type="text" id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}" style="visibility: hidden">
            
            <input type="submit" value="Modificar"/>
            </form> 
                
                <form id="modficarContrasena" action="./ModificarContrasena" method="post" >
                    Nueva contraseña: <br>
                    <input type="text" name="cedula" value="${usuario.cedula}" style="visibility: hidden"> 
                    <input type="text" name="contrasena"> <br>
                    <input type="submit" value="Cambiar contraseña"/>
                </form>
                
                <form id="eliminarCuenta" action="./eliminarusuario" method="post" >
                    <input type="text" name="cedula" value="${usuario.cedula}" style="visibility: hidden"> 
                    <input type="submit" value="EliminarCuenta"/>
                </form>
                    
                
        </div>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 
    </body>
</html>
