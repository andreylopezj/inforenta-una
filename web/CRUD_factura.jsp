<%-- 
    Document   : CRUD_factura
    Created on : 24-may-2020, 10:54:08
    Author     : DELL
--%>

<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%--Estilos--%>
       
       <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
       <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
       <link href="https://fonts.googleapis.com/css2?family=Rokkitt:wght@506&display=swap" rel="stylesheet">
       <link href="EstiloFactura.css" rel="stylesheet" type="text/css"/>
       <style> td {  font-family: 'Rokkitt', serif; }</style>
       <script src="js/OperacionesFactura.js" type="text/javascript"></script>
       
       <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    <body>
        
        <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
          
          
          
        <h1 class="text-center text-light"> <span class="badge badge-info">Facturas</span></h2> 
        
        <div class="contenedor" style="width:100%; margin: auto; overflow: scroll; height:600px;">
            <br><br><br><br>
            <%--inicializa la tabla--%>
            <label style="display: block;text-align: center;font-size: 25px;" >Listado de Facturas de Arrendamiento</label><br>
            
            <table id="tablaFacturas" class="table table-bordered">
                <th class="text-center">Número Factura</th>
                <th class="text-center">Número Contrato</th>
                <th class="text-center">Nombre Arrendador</th>
                <th class="text-center">Nombre Inquilino</th>
                <th class="text-center">Monto</th>
                <th class="text-center">Descripción</th>
                <th class="text-center">Fecha</th>
                <th class="text-center">Modalidad de Pago</th>
                <th class="text-center">Monto Total</th>
                <th class="text-center">Acciones</th>

                <tr>
                    <form method="post" action="./insertarfactura?nom=${usuario.nombre}">
                        <td>Nueva<input type="hidden" name="idFactura" value="1"/></td>
                        <td><input type="number" name="idContrato" placeholder="ej: código del contrato" id="idContrato" pattern="[1-10000]{1,8}$" class="required number" oninvalid="alert('Ingresa nuevamente el número de Contrato!');" required/></td>
                        <td><input type="text" name="nombreArrendador" value="${usuario.nombre}" oninvalid="alert('Ingresa nuevamente el nombre de Arrendador!');" readonly="readonly"/></td>
                        <td><input type="text" name="nombreInquilino" placeholder="ej: Yoselyn" id="nombreInquilino" pattern="[a-z A-Z]{3,50}$" class="required" oninvalid="alert('Ingresa nuevamente el nombre de Inquilino!');" required/></td>
                        <td><input type="number" name="monto" placeholder="ej: 250000" id="monto" pattern="[1-1000000]{4,7}$" class="required number" oninvalid="alert('Ingresa nuevamente el monto!');" required=""/></td>
                        <td><input type="text" name="descripcion" placeholder="ej: Concepto de la Factura" id="descripcion" class="required" oninvalid="alert('La descripcion ingresada no cumple con el formato!');" required/></td>
                        <td><input type="date" name="fecha" placeholder="ej: 2020-05-28" id="fecha"  class="required" oninvalid="alert('La fecha ingresado no cumple con el formato!');" required/></td>
                        <td><select name="modalidadPago" id="modalidadPago" class="required">
                                <option value="Efectivo">Efectivo</option>
                                <option value="Tarjeta">Tarjeta</option>
                            </select>
                        </td>
                        <td><input type="number" name="montoTotal" id="montoTotal" /></td>
                        <td><input type="submit" id="btnAgregar" name="btnAgregar"  value="Agregar" class="btn btn-primary btn-sm" /></td>
                    </form> 
                </tr>

                <c:forEach items="${listaFacturas}" var="factura">
                    <tr>
                        <td class="text-center"> <c:out value="${factura.idFactura}"/> </td>
                        <td class="text-center"> <c:out value="${factura.idContrato}"/> </td>
                        <td> <c:out value="${factura.nombreArrendador}"/> </td>
                        <td> <c:out value="${factura.nombreInquilino}"/> </td>
                        <td class="text-center"> <c:out value="${factura.monto}"/> </td>
                        <td> <div class="row_data" edit_type="click"> <c:out value="${factura.descripcion}"/> </div></td>
                        <td class="text-center"> <c:out value="${factura.fecha}"/> </td>
                        <td class="text-center"> <div class="row_data" edit_type="click"> <c:out value="${factura.modalidadPago}"/> </div></td>
                        <td class="text-center"> <c:out value="${factura.montoTotal}"/> </td>
                        <td class="text-center">
                            <a href="./CargarFactura?id=${factura.idFactura}" > Modificar</a>
                            <a href="./eliminarfactura?idFac=${factura.idFactura}&nom=${usuario.nombre}" class="btn btn-danger btn-sm">Eliminar</a>
                        </td>
                    </tr>
                </c:forEach>
                
            </table>
        </div>        
           
    </body>
</html>

