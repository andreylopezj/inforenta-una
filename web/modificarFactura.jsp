<%-- 
    Document   : modificar
    Created on : 20/06/2020, 11:27:39 PM
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    
    <body>
        
        <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
        
          <br><br><br><br>
          <h1>Modificar Factura #${factura.idFactura}</h1>
        <div class="col-md-3">
     <div class="card-body">
    <form action="./modificarfactura" method="POST">
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
          <th></th>
          </tr>
        </thead>
        <tbody>
          <th>
              <input type="hidden" name="idFactura" value="${factura.idFactura}" >
              # Contrato:<input type="text" name="idContrato" value="${factura.idContrato}"   class="form-control" readonly=»readonly»><br>
              Nombre del Arrendador:<input type="text" name="nombreArrendador" value="${factura.nombreArrendador}"  class="form-control" ><br>
              Nombre del Inquilino:<input type="text" name="nombreInquilino" value="${factura.nombreInquilino}"  class="form-control"><br>
              Monto:<input type="text" name="monto" value="${factura.monto}"  class="form-control"><br>
              Descripcion:<input type="text" name="descripcion" value="${factura.descripcion}"  class="form-control" autofocus><br>
              Fecha:<input type="text" name="fecha" value="${factura.fecha}"  class="form-control" autofocus readonly=»readonly»><br>
              Modalidad de pago:<input type="text" name="modalidadPago" value="${factura.modalidadPago}"  class="form-control"><br>
              Total:<input type="text" name="montoTotal" value="${factura.montoTotal}"  class="form-control"><br>
              <input type="hidden"  id="estado" name="estado" value="1" class="form-control"/>
              <input type="hidden"  id="nombre" name="nombre" value="${usuario.nombre}" class="form-control"/>
          </th>
          
        </tbody>
      </table>

      <button type="submit" class="btn btn-primary">Modificar</button>
    </form>
  </div>
</div>
    </body>
</html>
