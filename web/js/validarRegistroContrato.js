/* global Swal */

function validarContrato(){
    
    var nombre, telefono, idVivienda, fecha, monto, validezContrato,numeroCuenta,exprecionCedula,exprecionNumero;
    

    nombre = document.getElementById("nombre").value;
    telefono = document.getElementById("telefono").value;
    idVivienda = document.getElementById("idVivienda").value;
    fecha = document.getElementById("fecha").value;
    monto = document.getElementById("monto").value;
    validezContrato = document.getElementById("validezContrato").value;
    numeroCuenta = document.getElementById("numeroCuenta").value;
    
    exprecionNumero = /^[0-9]{1,8}$/;
    exprecionCedula = /^[1-9]\d{4}\d{4}$/;
    
    
    if(nombre === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Nombre esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    if(fecha === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Fecha esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(validezContrato === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo validez Contrato esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(telefono === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Télefono esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!exprecionNumero.test(telefono)){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El Télefono no tiene formato numérico!!',
            width: '25%'
          });
        return false;
    }
    
    else if(numeroCuenta === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo número Cuenta esta Vacio!',
            width: '25%'
          });
        return false;
    }
    else if(idVivienda === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo idVivienda esta Vacio!',
            width: '25%'
          });
        return false;
    }
    else if(!exprecionNumero.test(idVivienda)){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'La idVivienda no tiene formato numérico!!',
            width: '25%'
          });
        return false;
    }
    
    else if(monto === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo monto esta Vacio!',
            width: '25%'
          });
        return false;
    }
    else if(!exprecionNumero.test(monto)){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El Monto no tiene formato numérico!!',
            width: '25%'
          });
        return false;
    }
    
    else {
        Swal.fire({
            icon: 'success',
            title: 'Se registro correctamente',
            showConfirmButton: false,
            timer: 1500
        });
    }
    
    
}