/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function validarRegistro(){
    
    var nombre, apellidos, cedula, contrasena, telefono, correo, direccion, tipoUsuario, exprecionCorreo, exprecionNumero, exprecionCedula;
    
    nombre = document.getElementById("nombre").value;
    apellidos = document.getElementById("apellidos").value;
    cedula = document.getElementById("cedula").value;
    telefono = document.getElementById("telefono").value;
    correo = document.getElementById("correo").value;
    direccion = document.getElementById("direccion").value;
    
    exprecionNumero = /^[0-9]{1,8}$/;
    exprecionCedula = /^[1-9]\d{4}\d{4}$/;
    exprecionCorreo = /^[\w.+\-]+@gmail\.com$/;
    
    if(nombre === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Nombre esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(apellidos === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Apellidos esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(telefono === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Telefono esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!exprecionNumero.test(telefono)){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El Telefono no cumple con el formato establecido!!',
            width: '25%'
          });
        return false;
    }
    
    else if(correo === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Correo esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!exprecionCorreo.test(correo)){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El Correo no cumple con el formato establecido!!',
            width: '25%'
          });
        return false;
    }
    
    else if(direccion === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Direccion esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else {
        Swal.fire({
            icon: 'success',
            title: 'Registro Existoso',
            showConfirmButton: false,
            timer: 1500
        });
    }
    

    
}
