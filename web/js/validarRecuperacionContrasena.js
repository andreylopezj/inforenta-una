/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global Swal */
function validarDatos(){
    
    var contrasena, contrasena2, clave;
    
    clave = document.getElementById("nclave").value;
    contrasena = document.getElementById("ncontrasena").value;
    contrasena2 = document.getElementById("ncontrasena2").value;

    // validacion de que los campos no esten vacios
    if (contrasena2 !== contrasena){
        Swal.fire({
            icon: 'error',
            title: 'Contraseñas distintas',
            text: 'Verifique los campos de texto',
            width: '25%'
          });
        return false;
    }
    
    else if (contrasena.length >= 5){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'La contraseña es demasiado corta!',
            width: '25%'
          });
        return false;
    }
    
    else if(clave.length <= 5){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'La clave es demasiado corta!',
            width: '25%'
          });
        return false;
    }
    
    else{
        Swal.fire({
            icon: 'success',
            title: 'Actualizacion Existosa',
            showConfirmButton: false,
            timer: 1500
        });
    }
}
