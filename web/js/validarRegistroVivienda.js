/* global Swal */

function validar(){
    
    var direccion, niseagua, niseluz, precio, imagenes, tipoVivienda, cantCuartos, cantBanios, dimensiones, expresion;
    
    direccion = document.getElementById("direccionVivienda").value;
    niseagua = document.getElementById("niseAgua").value;
    niseluz = document.getElementById("niseLuz").value;
    precio = document.getElementById("precio").value;
    imagenes = document.getElementById("archivos").value;
    tipoVivienda = document.getElementById("tipoVivienda").value;
    cantCuartos = document.getElementById("cantCuartos").value;
    cantBanios = document.getElementById("cantBanos").value;
    dimensiones = document.getElementById("dimensiones").value;
    
    expresion = /^[0-9]{1,11}$/;

    // validacion de que los campos no esten vacios
    if (direccion === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Direccion Vivienda esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if (niseagua === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo NISE Agua esta Vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!expresion.test(niseagua) || niseagua.length > 11){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo NISE Agua no cumple con el formato, Ingrese solo numeros y no mas de 11 digitos!',
            width: '25%'
          });
        return false;
    }
    
    else if (niseluz === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo NISE Luz esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!expresion.test(niseluz) || niseluz.length > 11){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo NISE Luz no cumple con el formato, Ingrese solo numeros y no mas de 11 digitos!',
            width: '25%'
          });
        return false;
    }
    
    else if (precio === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Precio esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!expresion.test(precio) || precio.length > 11){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Precio no cumple con el formato, Ingrese solo numeros y no mas de 11 digitos!',
            width: '25%'
          });
        return false;
    }
    
    else if (imagenes === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Imagenes esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if (tipoVivienda === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Tipo Vivienda esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('internet1').checked && !document.getElementById('internet2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Internet esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('ninos1').checked && !document.getElementById('ninos2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Niños esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('cable1').checked && !document.getElementById('cable2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Cable esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('mascotas1').checked && !document.getElementById('mascotas2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Mascotas esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('garage1').checked && !document.getElementById('garage2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Garage esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('amueblado1').checked && !document.getElementById('amueblado2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Amueblado esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('zonaverde1').checked && !document.getElementById('zonaverde2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Zona Verde esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('cuartopila1').checked && !document.getElementById('cuartopila2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Cuarto Pila esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('seguridad1').checked && !document.getElementById('seguridad2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Sistema Seguridad esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!document.getElementById('visitas1').checked && !document.getElementById('visitas2').checked){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Visitas esta vacio!',
            width: '25%'
          });
        return false;
    } 
    
    else if (cantCuartos === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Cantidad de Cuartos esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!expresion.test(cantCuartos) || cantCuartos.length > 11){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Cantidad Cuartos no cumple con el formato, Ingrese solo numeros y no mas de 11 digitos!',
            width: '25%'
          });
        return false;
    }
    
    else if (cantBanios === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Cantidad de Baños esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else if(!expresion.test(cantBanios) || cantBanios.length > 11){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Cantidad Baños no cumple con el formato, Ingrese solo numeros y no mas de 11 digitos!',
            width: '25%'
          });
        return false;
    }
    
    else if (dimensiones === ""){
        Swal.fire({
            icon: 'error',
            title: 'ERROR...',
            text: 'El campo Dimensiones esta vacio!',
            width: '25%'
          });
        return false;
    }
    
    else{
        Swal.fire({
            icon: 'success',
            title: 'Registro Existoso',
            showConfirmButton: false,
            timer: 1500
        });
    }
}

function exito(){
    //alert("se registro!!");
    Swal.fire({
  icon: 'success',
  title: 'Your work has been saved',
  showConfirmButton: false,
  timer: 2500
});
}