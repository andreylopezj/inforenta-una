<%@page import="cr.ac.una.inforenta.negocio.ControlAcceso"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrar Lista Contratos</title>
        <link rel="stylesheet" type="text/css" href="EstiltrarUsuarioListado.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        
        <link rel="stylesheet" type="text/css" href="EstiloIndex.css">
        <script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="js/validarMenu.js"></script>
    </head>
    
    <body>

         <%new ControlAcceso(request.getSession(),response).validarAcceso();%>
          <input type="hidden"  id="tipoUsuario" name="tipoUsuario" value="${usuario.tipoUsuario}"/>                   
          <input type="hidden" id="nombre" name="nombre" value="${usuario.nombre} ${usuario.apellidos}"/>
          <input type="hidden" id="cedula" name="cedula" value="${usuario.cedula} "/>
          <input type="hidden" id="nom" name="nom" value="${usuario.nombre} "/>
          
          <header id="header" class="header"></header>
          <script> mostrarMenu();</script> 
        
          <div style="padding-left:10%; padding-right:10%; margin: auto; overflow: scroll; height:670px;" >
            <br><br><br><br><br>
            <h1 id="titulo_MostraarioListado">Mostrar Lista Contratos</h1>
             
            <form id="formulario_MostrrioListado" action="index.html">
             <a class="btn btn-success btn-lg" href="./comboboxinquilino">Nuevo Registro</a><br><br>
            <table border="3">
                  
            <th>idContrato</th>
            <th>idUsuarioArrendador</th>
            <th>nombreArrendador</th>
            <th>telefono</th>
            <th>datos_inquilino</th>
            <th>idVivienda</th>
            <th>fecha</th>
            <th>monto</th>
            <th>cuentaBancaria</th>
            <th>plazoValidezContrato</th>
            <th>Acciones</th>
                        
            <c:forEach items="${listaContratos}" var="contrato">
                <tr>
                    <td> <c:out value="${contrato.idContrato}"/> </td>
                    <td> <c:out value="${contrato.idArrendador}"/> </td>
                    <td> <c:out value="${contrato.nombreArrendador}"/> </td>
                    <td> <c:out value="${contrato.telefono}"/> </td>
                    <td> <c:out value="${contrato.datos_inquilino}"/> </td>
                    <td> <c:out value="${contrato.idVivienda}"/> </td>
                    <td> <c:out value="${contrato.fecha}"/> </td>
                    <td> <c:out value="${contrato.monto}"/> </td>
                    <td> <c:out value="${contrato.cuentaBancaria}"/> </td>
                    <td> <c:out value="${contrato.plazoValidezContrato}"/> </td>
                    
                    <td>
                        <a href="Modificar_Contrato.jsp?idContrato=${contrato.idContrato}" class="btn btn-warning btn-sm">Editar</a>
                        <a href="./eliminarcontrato?idContrato=${contrato.idContrato}&ced=${usuario.cedula}" class="btn btn-danger btn-sm">Eliminar</a>
                    </td>
                </tr>
                
            </c:forEach>
            
        </table>
                    
                       
           
        </div>   
        
    </body>
</html>
