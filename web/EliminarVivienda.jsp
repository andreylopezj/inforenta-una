
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cr.ac.una.inforenta.servlet.EliminarViviendaServlet"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar Vivienda</title>
    </head>
    
    <body>
        
        <form class="formularioLibro" method="post" action="./EliminarVivienda">
            <h3>Eliminar Vivienda</h3>
            
            <label >Seleccione vivienda a eliminar:</label><br>
            <select name="idVivienda">
                <c:forEach items="${listaViviendas}" var="tempVivienda">             
                    <option value="${tempVivienda.idVivienda}"> Vivienda #${tempVivienda.idVivienda} </option>
                </c:forEach>
            </select>
            
            <input type="hidden" name="indiceEliminar" value="2"/>
            <input id="bnRegistro" type="submit" value="Eliminar"/><br/>
            <a href="index.html" > Atras</a>
            
        </form>
        
    </body>
</html>
