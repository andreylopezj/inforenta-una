
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrar Usuario Listado</title>
        <link rel="stylesheet" type="text/css" href="EstiloMostrarUsuarioListado.css">
    </head>
    
    <body>        
        <div id="cuadroMostraUsuarioListado">
            <h1 id="titulo_MostraUsuarioListado">Mostrar Usuario Listado</h1>

            <form id="formulario_MostraUsuarioListado" action="index.html">
            <table border="3">
                  
            <th>Cedula</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Telefono</th>
            <th>Direccion</th>
            <th>Correo</th>
            
            <c:forEach items="${listaUsuarios}" var="usuario">
                <tr>
                    <td> <c:out value="${usuario.cedula}"/> </td>
                    <td> <c:out value="${usuario.nombre}"/> </td>
                    <td> <c:out value="${usuario.apellidos}"/> </td>
                    <td> <c:out value="${usuario.telefono}"/> </td>
                    <td> <c:out value="${usuario.direccion}"/> </td>
                    <td> <c:out value="${usuario.correo}"/> </td>
                </tr>
                
            </c:forEach>
            
        </table>

                        
            </form>
        </div>        
    </body>
</html>
