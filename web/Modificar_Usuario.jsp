<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Usuario</title>
        <link rel="stylesheet" type="text/css" href="EstiloModificarUsuario.css">
    </head>
    
    <body>
                
        <div id="cuadroModificarUsuario">
            <h1 id="titulo_ModificarUsuario">Modificar Usuario</h1>
            
            <form id="formulario_ModificarUsuario" method="post" action="./modificarusuario">
                Cédula del Usuario a modificar  <input type="text" name="cedula" required/><br/><br/><br/>
              
                Telefono: <br>
                <input type="text" name="telefono"> <br><br>

                Correo: <br>
                <input type="text" name="correo"> <br><br>

                Direccion: <br>
                <input type="text" name="direccion"> <br><br>
            
            <input type="submit" value="Modificar"/>
            </form>            
        </div>
        
    </body>
</html>