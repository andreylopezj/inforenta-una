
package cr.ac.una.inforenta.dominio;

public class Vivienda {

    private int idVivienda;
    private String direccion;
    private int niceAgua;
    private int niceElectricidad;
    private int precio;
    private String imagen1;
    private String imagen2;
    private String imagen3;
    private String imagen4;
    private String imagen5;
    private int idDescripcion;

    // Constructor con todos los parametros
    public Vivienda(int idVivienda, String direccion, int niceAgua, int niceElectricidad, int precio, String imagen1, String imagen2, 
            String imagen3, String imagen4, String imagen5, int idDescripcion) {
        this.idVivienda = idVivienda;
        this.direccion = direccion;
        this.niceAgua = niceAgua;
        this.niceElectricidad = niceElectricidad;
        this.precio = precio;
        this.imagen1 = imagen1;
        this.imagen2 = imagen2;
        this.imagen3 = imagen3;
        this.imagen4 = imagen4;
        this.imagen5 = imagen5;
        this.idDescripcion = idDescripcion;
    }

    public Vivienda(int idVivienda, String direccion, int niceAgua, int niceElectricidad, int precio) {
        this.idVivienda = idVivienda;
        this.direccion = direccion;
        this.niceAgua = niceAgua;
        this.niceElectricidad = niceElectricidad;
        this.precio = precio;
    }

    // Constructor sin parametros
    public Vivienda() { }

    
    // metodos Getter y Setter
    
    public int getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(int idVivienda) {
        this.idVivienda = idVivienda;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNiceAgua() {
        return niceAgua;
    }

    public void setNiceAgua(int niceAgua) {
        this.niceAgua = niceAgua;
    }

    public int getNiceElectricidad() {
        return niceElectricidad;
    }

    public void setNiceElectricidad(int niceElectricidad) {
        this.niceElectricidad = niceElectricidad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getImagen1() {
        return imagen1;
    }

    public void setImagen1(String imagen1) {
        this.imagen1 = imagen1;
    }

    public String getImagen2() {
        return imagen2;
    }

    public void setImagen2(String imagen2) {
        this.imagen2 = imagen2;
    }

    public String getImagen3() {
        return imagen3;
    }

    public void setImagen3(String imagen3) {
        this.imagen3 = imagen3;
    }

    public String getImagen4() {
        return imagen4;
    }

    public void setImagen4(String imagen4) {
        this.imagen4 = imagen4;
    }

    public String getImagen5() {
        return imagen5;
    }

    public void setImagen5(String imagen5) {
        this.imagen5 = imagen5;
    }

    public int getIdDescripcion() {
        return idDescripcion;
    }

    public void setIdDescripcion(int idDescripcion) {
        this.idDescripcion = idDescripcion;
    }

    @Override
    public String toString() {
        return "Vivienda{" + "idVivienda=" + idVivienda + ", direccion=" + direccion + ", niceAgua=" + niceAgua + ", niceElectricidad=" + 
                niceElectricidad + ", precio=" + precio + ", imagen1=" + imagen1 + ", imagen2=" + imagen2 + ", imagen3=" + imagen3 + ", imagen4=" + 
                imagen4 + ", imagen5=" + imagen5 + ", idDescripcion=" + idDescripcion + '}';
    }
    
    
}
