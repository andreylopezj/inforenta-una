
package cr.ac.una.inforenta.dominio;

public class Login {
    
    private int cedula;
    private String contrasena;
    
    public Login(int cedula, String contrasena) {
        this.cedula = cedula;
        this.contrasena = contrasena;
    }
    
    public Login() {
    }
    
    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    
    @Override
    public String toString() {
        return "Login{" + "cedula=" + cedula + ", contrasena=" + contrasena + '}';
    }
    
    
}
