/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.dominio;

/**
 *
 * @author DELL
 */
public class Factura {
    
    int idFactura;
    int idContrato;
    String nombreArrendador;
    String nombreInquilino;
    int monto;
    String descripcion;
    String fecha;
    String modalidadPago;
    int montoTotal;
    int estado;

    public Factura(){}

    public Factura(int idFactura, String descripcion, String modalidadPago) {
        this.idFactura = idFactura;
        this.descripcion = descripcion;
        this.modalidadPago = modalidadPago;
    }
    
    public Factura(int idFactura, int idContrato, String nombreArrendador, String nombreInquilino, int monto, String descripcion, String fecha, String modalidadPago, int montoTotal) {
        this.idFactura = idFactura;
        this.idContrato = idContrato;
        this.nombreArrendador = nombreArrendador;
        this.nombreInquilino = nombreInquilino;
        this.monto = monto;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.modalidadPago = modalidadPago;
        this.montoTotal = montoTotal;
    }
    
    public Factura(int idFactura, int idContrato, String nombreArrendador, String nombreInquilino, int monto, String descripcion, String fecha,String modalidadPago, int montoTotal, int estado) {
        this.idFactura = idFactura;
        this.idContrato = idContrato;
        this.nombreArrendador = nombreArrendador;
        this.nombreInquilino = nombreInquilino;
        this.monto = monto;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.modalidadPago = modalidadPago;
        this.montoTotal = montoTotal;
        this.estado = estado;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public String getNombreArrendador() {
        return nombreArrendador;
    }

    public void setNombreArrendador(String nombreArrendador) {
        this.nombreArrendador = nombreArrendador;
    }

    public String getNombreInquilino() {
        return nombreInquilino;
    }

    public void setNombreInquilino(String nombreInquilino) {
        this.nombreInquilino = nombreInquilino;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getModalidadPago() {
        return modalidadPago;
    }

    public void setModalidadPago(String modalidadPago) {
        this.modalidadPago = modalidadPago;
    }

    public int getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(int montoTotal) {
        this.montoTotal = montoTotal;
    }    
    
}
