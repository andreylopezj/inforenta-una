
package cr.ac.una.inforenta.dominio;

public class Descripcion {

    private int idDescripcion;
    private int idVivienda;
    private int internet;
    private int cable;
    private int mascotas;
    private int garage;
    private int zonaVerde;
    private int cuartoPila;
    private String dimensiones;
    private int ninos;
    private int cantCuartos;
    private String tipoVivienda;
    private int amueblado;
    private int cantBanos;
    private int sistemaSeguridad;
    private int visitas;

    public Descripcion(int idDescripcion, int idVivienda, int internet, int cable, int mascotas, int garage, int zonaVerde, int cuartoPila, 
            String dimensiones, int ninos, int cantCuartos, String tipoVivienda, int amueblado, int cantBanos, int sistemaSeguridad, int visitas) {
        this.idDescripcion = idDescripcion;
        this.idVivienda = idVivienda;
        this.internet = internet;
        this.cable = cable;
        this.mascotas = mascotas;
        this.garage = garage;
        this.zonaVerde = zonaVerde;
        this.cuartoPila = cuartoPila;
        this.dimensiones = dimensiones;
        this.ninos = ninos;
        this.cantCuartos = cantCuartos;
        this.tipoVivienda = tipoVivienda;
        this.amueblado = amueblado;
        this.cantBanos = cantBanos;
        this.sistemaSeguridad = sistemaSeguridad;
        this.visitas = visitas;
    }

    public Descripcion(int idVivienda, int internet, int cable, int mascotas, int garage, int zonaVerde, 
            int cuartoPila, String dimensiones, int ninos, int cantCuartos, String tipoVivienda, int amueblado, 
            int cantBanos, int sistemaSeguridad, int visitas) {
        this.idVivienda = idVivienda;
        this.internet = internet;
        this.cable = cable;
        this.mascotas = mascotas;
        this.garage = garage;
        this.zonaVerde = zonaVerde;
        this.cuartoPila = cuartoPila;
        this.dimensiones = dimensiones;
        this.ninos = ninos;
        this.cantCuartos = cantCuartos;
        this.tipoVivienda = tipoVivienda;
        this.amueblado = amueblado;
        this.cantBanos = cantBanos;
        this.sistemaSeguridad = sistemaSeguridad;
        this.visitas = visitas;
    }
    
    

    public Descripcion() {
    }

    public int getIdDescripcion() {
        return idDescripcion;
    }

    public void setIdDescripcion(int idDescripcion) {
        this.idDescripcion = idDescripcion;
    }
        
    public int getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(int idVivienda) {
        this.idVivienda = idVivienda;
    }

    public int getInternet() {
        return internet;
    }

    public void setInternet(int internet) {
        this.internet = internet;
    }    

    public int getCable() {
        return cable;
    }

    public void setCable(int cable) {
        this.cable = cable;
    }

    public int getMascotas() {
        return mascotas;
    }

    public void setMascotas(int mascotas) {
        this.mascotas = mascotas;
    }

    public int getGarage() {
        return garage;
    }

    public void setGarage(int garage) {
        this.garage = garage;
    }

    public int getZonaVerde() {
        return zonaVerde;
    }

    public void setZonaVerde(int zonaVerde) {
        this.zonaVerde = zonaVerde;
    }

    public int getCuartoPila() {
        return cuartoPila;
    }

    public void setCuartoPila(int cuartoPila) {
        this.cuartoPila = cuartoPila;
    }

    public String getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }

    public int getNinos() {
        return ninos;
    }

    public void setNinos(int ninos) {
        this.ninos = ninos;
    }

    public int getCantCuartos() {
        return cantCuartos;
    }

    public void setCantCuartos(int cantCuartos) {
        this.cantCuartos = cantCuartos;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public int getAmueblado() {
        return amueblado;
    }

    public void setAmueblado(int amueblado) {
        this.amueblado = amueblado;
    }

    public int getCantBanos() {
        return cantBanos;
    }

    public void setCantBanos(int cantBanos) {
        this.cantBanos = cantBanos;
    }

    public int getSistemaSeguridad() {
        return sistemaSeguridad;
    }

    public void setSistemaSeguridad(int sistemaSeguridad) {
        this.sistemaSeguridad = sistemaSeguridad;
    }

    public int getVisitas() {
        return visitas;
    }

    public void setVisitas(int visitas) {
        this.visitas = visitas;
    }

    @Override
    public String toString() {
        return "Descripcion{" + "idDescripcion=" + idDescripcion + "idVivienda=" + idVivienda + ", cable=" + cable + ", mascotas=" + mascotas + 
                ", garage=" + garage + ", zonaVerde=" + zonaVerde + ", cuartoPila=" + cuartoPila + ", dimensiones=" + dimensiones + ", ninos=" + 
                ninos + ", cantCuartos=" + cantCuartos + ", tipoVivienda=" + tipoVivienda + ", amueblado=" + amueblado + ", cantBanos=" + cantBanos + 
                ", sistemaSeguridad=" + sistemaSeguridad + ", vivistas=" + visitas + '}';
    }
}
