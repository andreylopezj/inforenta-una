package cr.ac.una.inforenta.dominio;	

public class Contrato {
    
    private int idContrato;
    private String fecha;
    private float monto;
    private String cuentaBancaria;
    private String plazoValidezContrato;
    //Datos Arrendador
    private int idArrendador;
    private String nombreArrendador;
    private int telefono;
    //Datos Inquilino
    private String datos_inquilino;
    //Datos vivienda
    private int idVivienda;
    
    //Objetos de composicion
    private Usuario usuario;
    private Vivienda vivienda;


    public Contrato() {
        this.usuario = usuario;
        this.vivienda = vivienda;
    }
    public Contrato(int idContrato,int telefono,float monto, String plazoValidezContrato, String cuentaBancaria){
        this.idContrato = idContrato;
        this.telefono = telefono;
        this.monto = monto;
        this.cuentaBancaria = cuentaBancaria;
        this.plazoValidezContrato = plazoValidezContrato;
    }
    
    public Contrato(int idContrato,int idArrendador, String nombreArrendador, int telefono, String datos_inquilino,int idVivienda,String fecha, float monto, String cuentaBancaria, String plazoValidezContrato ) {
        this.idContrato = idContrato;
         this.idArrendador = idArrendador;
        this.nombreArrendador = nombreArrendador;
        this.telefono = telefono;
        this.datos_inquilino = datos_inquilino;
        this.fecha = fecha;
        this.monto = monto;
        this.cuentaBancaria = cuentaBancaria;
        this.plazoValidezContrato = plazoValidezContrato;
       
    }
    
    

    public Contrato(int idContrato, String fecha, float monto, String cuentaBancaria, String plazoValidezContrato, int idArrendador, String nombreArrendador, int telefono, String datos_inquilino, int idVivienda) {
        this.idContrato = idContrato;
        this.fecha = fecha;
        this.monto = monto;
        this.cuentaBancaria = cuentaBancaria;
        this.plazoValidezContrato = plazoValidezContrato;
        this.idArrendador = idArrendador;
        this.nombreArrendador = nombreArrendador;
        this.telefono = telefono;
        this.datos_inquilino = datos_inquilino;
        this.idVivienda = idVivienda;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    public String getPlazoValidezContrato() {
        return plazoValidezContrato;
    }

    public void setPlazoValidezContrato(String plazoValidezContrato) {
        this.plazoValidezContrato = plazoValidezContrato;
    }

    public int getIdArrendador() {
        return idArrendador;
    }

    public void setIdArrendador(int idArrendador) {
        this.idArrendador = idArrendador;
    }

    public String getNombreArrendador() {
        return nombreArrendador;
    }

    public void setNombreArrendador(String nombreArrendador) {
        this.nombreArrendador = nombreArrendador;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDatos_inquilino() {
        return datos_inquilino;
    }

    public void setDatos_inquilino(String datos_inquilino) {
        this.datos_inquilino = datos_inquilino;
    }

    public int getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(int idVivienda) {
        this.idVivienda = idVivienda;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Vivienda getVivienda() {
        return vivienda;
    }

    public void setVivienda(Vivienda vivienda) {
        this.vivienda = vivienda;
    }

    
    
    
    
    
}
