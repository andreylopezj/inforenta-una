
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Descripcion;
import cr.ac.una.inforenta.dominio.Vivienda;
import cr.ac.una.inforenta.negocio.LogicaDescripcion;
import cr.ac.una.inforenta.negocio.LogicaVivienda;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;



public class InsertarViviendaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        // Clases para almacenamiento de las imagenes
        FileItemFactory file_factory = new DiskFileItemFactory();
        ServletFileUpload sfu = new ServletFileUpload(file_factory);
                
        // se crean 2 arrayList para almacenar una lista con los imput y otro para los de tipo archivos
        ArrayList<String> campos = new ArrayList<>();
        ArrayList<String> imgs = new ArrayList<>();
        
        // se hace un for para recorrer la lista con los request del formulario del jsp y insertarlos en los arrayLis correspondientes
        try{
            List items = sfu.parseRequest(request);
            System.out.println("\n\nTamaño de la lista!!  "+items.size());
            for (int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);
                if(!item.isFormField()){    // si el dato es de tipo archivo
                    // ruta del file para guardar las imagenes en la carpeta del proeycto
                    File archivo = new File("D:\\Adrian-PC\\Escritorio\\.Cursos UNA\\III nivel\\I Semestre\\Progra 4\\Repositorio proyecto "+
                            "infoRenta\\inforenta-una\\web\\imgAlquiler\\"+item.getName());
                    item.write(archivo);
                    imgs.add("imgAlquiler/"+item.getName()); // se agregan a la lista de archivos con el nombre y la carpeta que la contiene
                }else{  // si son de tipo input
                    campos.add(item.getString());
                }
            }
        }catch(Exception ex){
        }
        
        LogicaDescripcion logDescripcion = new LogicaDescripcion();
        LogicaVivienda logVivienda = new LogicaVivienda();
        String ruta = "";
                
        try {
            try {
                /* primero se obtiene el ultimo id de vivienda y de descripcion, luego se crean las clases.
                   para la cracion de las clases se usan los datos de los arrayList que contienen los request del jsp
                */
                
                int idDescripcion = logDescripcion.ultimoId()+1;
                int idVivienda = logVivienda.ultimoIdVivienda()+1;
                
                // orden de la clase vivienda(idVivienda, direccion, niceAgua, luz, precio, img1. img2, img3, img4,img5,idDescripcion)
                Vivienda vivienda = new Vivienda(idVivienda, 
                        campos.get(0),                       // direccion
                        Integer.parseInt(campos.get(1)),    // niceAgua
                        Integer.parseInt(campos.get(2)),    // niceLuz
                        Integer.parseInt(campos.get(3)),    // precio
                        imgs.get(0), imgs.get(1), imgs.get(2), imgs.get(3), imgs.get(4),    // imagenes
                        idDescripcion);                     // descripcion
                
                /*  orden de la clase descripcion( int idDescripcion, int idVivienda, int internet, int cable, int mascotas, int garage, int zonaVerde, 
                    int cuartoPila, String dimensiones, int ninos, int cantCuartos, String tipoVivienda, int amueblado, int cantBanos, 
                    int sistemaSeguridad, int visitas
                */
                Descripcion descripcion = new Descripcion(idDescripcion, idVivienda, 
                        Integer.parseInt(campos.get(4)),    // internet
                        Integer.parseInt(campos.get(6)),    // cable
                        Integer.parseInt(campos.get(7)),    // mascotas
                        Integer.parseInt(campos.get(8)),    // garage
                        Integer.parseInt(campos.get(10)),   // zonaVerde
                        Integer.parseInt(campos.get(11)),   // cuartopila
                        campos.get(17),                     // dimension
                        Integer.parseInt(campos.get(5)),    // niños
                        Integer.parseInt(campos.get(14)),   // cantCuartos
                        campos.get(15),                     // tipoVivienda
                        Integer.parseInt(campos.get(9)),    // amueblado
                        Integer.parseInt(campos.get(16)),   // canBaños
                        Integer.parseInt(campos.get(12)),   // seguridad
                        Integer.parseInt(campos.get(13)));  // visitas);
                                
                logVivienda.insertarVivenda(vivienda);
                System.out.println("\nSe Registro una vivienda");                    
                    
                logDescripcion.insertarDescripcion(descripcion);
                System.out.println("\nSe Registro una descripcion");
                
                ruta = "Registro_Vivienda.jsp";                          
                
            } catch (ClassNotFoundException ex){
                Logger.getLogger(InsertarViviendaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InsertarViviendaServlet.class.getName()).log(Level.SEVERE, null, ex);
            ruta= "no_exito.jsp";
        }
        RequestDispatcher despachador= request.getRequestDispatcher(ruta);
        despachador.forward(request, response);
    }
    
    
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
