
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Vivienda;
import cr.ac.una.inforenta.negocio.LogicaVivienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CargarIndexServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        
        LogicaVivienda lvivienda = new LogicaVivienda();
        RequestDispatcher despachador = null;
        
        switch (opcion) {
            case 1:
                despachador= request.getRequestDispatcher("index.jsp");
                break;
            case 2:
                despachador= request.getRequestDispatcher("MenuInquilino.jsp");
                break;
            case 3:
                despachador= request.getRequestDispatcher("MenuArrendador.jsp");
                break;
            case 4:
                despachador= request.getRequestDispatcher("MenuAdministrador.jsp");
                break;
            default:
                break;
        }
        
        LinkedList<Vivienda> lista = lvivienda.getViviendas();
        request.setAttribute("listaviviendas", lista);
        despachador.forward(request, response);
        
        for(Vivienda v:lista){
            System.out.println(v);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
