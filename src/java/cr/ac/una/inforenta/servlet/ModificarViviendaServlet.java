/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.datos.DataDescripcion;
import cr.ac.una.inforenta.datos.DataVivienda;
import cr.ac.una.inforenta.dominio.Descripcion;
import cr.ac.una.inforenta.dominio.Vivienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andre
 */
public class ModificarViviendaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        //Datos de Vivienda
        int idVivienda = Integer.parseInt(request.getParameter("idVivienda"));
        String direccionVivienda= request.getParameter("direccionVivienda");
        int niseAgua = Integer.parseInt(request.getParameter("niseAgua"));
        int niseLuz = Integer.parseInt(request.getParameter("niseLuz"));
        int precio = Integer.parseInt(request.getParameter("precio"));
        
        //Datos de Descripcion de vivienda
        int internet = Integer.parseInt(request.getParameter("internet"));
        int ninos = Integer.parseInt(request.getParameter("ninos"));
        int cable = Integer.parseInt(request.getParameter("cable"));
        int mascotas = Integer.parseInt(request.getParameter("mascotas"));
        int garage = Integer.parseInt(request.getParameter("garage"));
        int amueblado = Integer.parseInt(request.getParameter("amueblado"));
        int zonaverde = Integer.parseInt(request.getParameter("zonaverde"));
        int cuartopila = Integer.parseInt(request.getParameter("cuartopila"));
        int seguridad = Integer.parseInt(request.getParameter("seguridad"));
        int visitas = Integer.parseInt(request.getParameter("visitas"));
        int cantCuartos = Integer.parseInt(request.getParameter("cantCuartos"));
        String tipoVivienda = request.getParameter("tipoVivienda");
        int cantBanos = Integer.parseInt(request.getParameter("cantBanos"));
        String dimensiones = request.getParameter("dimensiones");
        
        String ruta= "MostrarVivienda.jsp";
        
        Vivienda vivienda = new Vivienda(idVivienda, direccionVivienda, niseAgua, niseLuz, precio);
        System.out.println(vivienda.toString());
        DataVivienda dVivienda = new DataVivienda();
        
        Descripcion descripcion = new Descripcion(idVivienda, internet, cable, mascotas, garage, zonaverde, cuartopila, 
                dimensiones, ninos, cantCuartos, tipoVivienda, amueblado, cantBanos, seguridad, visitas);
        DataDescripcion dDescripcion = new DataDescripcion();
        
        try {
            dVivienda.actualizarVivienda(vivienda);
            dDescripcion.actualizarDescripcion(descripcion);
        } catch (SQLException ex) {
            Logger.getLogger(ModificarViviendaServlet.class.getName()).log(Level.SEVERE, null, ex);
            ruta = "no_exito.jsp";
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ModificarViviendaServlet.class.getName()).log(Level.SEVERE, null, ex);
            ruta = "no_exito.jsp";
        }
        
        RequestDispatcher despachador= request.getRequestDispatcher(ruta);
        despachador.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
