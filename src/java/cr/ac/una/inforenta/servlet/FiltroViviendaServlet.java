
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.datos.DataVivienda;
import cr.ac.una.inforenta.dominio.Vivienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FiltroViviendaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String precio = request.getParameter("precioVivienda");
        int cuartos = Integer.parseInt(request.getParameter("cantCuartos"));
        
        System.out.println("\n\t\t\t\t\t precio: "+precio+" ---------- cuartos: "+cuartos);
        
        DataVivienda dvivienda = new DataVivienda();
        LinkedList<Vivienda> lista = new LinkedList<>();
        
        if(!precio.equals("")  && cuartos >= 1){
            lista = dvivienda.getFiltro_CuartosPrecio(cuartos, Integer.parseInt(precio));
        }
        else if(!precio.equals("")){
            lista = dvivienda.getFiltro_Precio(Integer.parseInt(precio));
        }
        else if(cuartos>=1){
            lista = dvivienda.getFiltro_Cuartos(cuartos);
        }
        request.setAttribute("listaviviendas", lista);
        RequestDispatcher despachador= request.getRequestDispatcher("index.jsp");
        despachador.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
