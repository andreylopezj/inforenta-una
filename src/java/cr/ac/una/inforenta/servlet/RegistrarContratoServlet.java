package cr.ac.una.inforenta.servlet;


import cr.ac.una.inforenta.dominio.Contrato;
import cr.ac.una.inforenta.dominio.Usuario;
import cr.ac.una.inforenta.dominio.Vivienda;
import cr.ac.una.inforenta.negocio.LogicaContrato;
import cr.ac.una.inforenta.negocio.LogicaUsuario;
import cr.ac.una.inforenta.negocio.LogicaVivienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrarContratoServlet extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
       
        PrintWriter out = response.getWriter(); 
        //Obtiene los datos del .jsp
        int idVivienda = Integer.parseInt(request.getParameter("idVivienda"));
        int cedulaArrendador = Integer.parseInt(request.getParameter("cedula"));
        String nombreArrendador = request.getParameter("nombre");
        int telefono = Integer.parseInt(request.getParameter("telefono"));   
        String datos_inquilino = request.getParameter("datos_inquilino");
        String fecha = request.getParameter("fecha");
        int monto = Integer.parseInt(request.getParameter("monto"));
        String validezContrato = request.getParameter("validezContrato");
        String numeroCuenta = request.getParameter("numeroCuenta");
               
            LogicaContrato lContrato= new LogicaContrato();
            LogicaUsuario lUsuario = new LogicaUsuario();
            LogicaVivienda lVivienda = new LogicaVivienda();
            Usuario usuario= new Usuario();
            Vivienda vivienda = new Vivienda();
            
        
        String ruta = "";
        try {
            try{
                int idContrato = lContrato.ultimoIdContrato()+1;
                
                usuario = lUsuario.getUsuario(cedulaArrendador);
                vivienda = lVivienda.getVivienda(idVivienda);
                
                if(vivienda != null){
                    System.out.println("arrendador "+usuario.getTipoUsuario()+"**********"+usuario.getCedula()+"=================");
                Contrato contrato= new Contrato(idContrato,fecha, monto,numeroCuenta,validezContrato,cedulaArrendador,
                       nombreArrendador,telefono,datos_inquilino,idVivienda);
                lContrato.insertarContratos(contrato);
       
                RequestDispatcher despachador= request.getRequestDispatcher("Mostrar_Contratos.jsp");
                LinkedList<Contrato> lista= lContrato.getContratos(cedulaArrendador);
                request.setAttribute("listaContratos", lista);
                despachador.forward(request, response);
               
                }            
           
                    }catch(ClassNotFoundException ex){
                 Logger.getLogger(RegistrarContratoServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistrarContratoServlet.class.getName()).log(Level.SEVERE, null, ex);
             ruta="p.jsp";             
        }
        
        
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
