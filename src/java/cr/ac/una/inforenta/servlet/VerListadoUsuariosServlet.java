package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Usuario;
import cr.ac.una.inforenta.negocio.LogicaUsuario;
import java.io.IOException;
import java.util.LinkedList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class VerListadoUsuariosServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        LogicaUsuario lUsuario= new LogicaUsuario();
        RequestDispatcher despachador= request.getRequestDispatcher("Mostrar_UsuarioListado.jsp");
        
        int tipo= 2;//¿arreglar con Login?
        
        LinkedList<Usuario> lista= lUsuario.getUsuarios(tipo);
        request.setAttribute("listaUsuarios", lista);
        despachador.forward(request, response);
    }

    //¿Para que son estos metodos?
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
