
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Descripcion;
import cr.ac.una.inforenta.dominio.Vivienda;
import cr.ac.una.inforenta.negocio.LogicaDescripcion;
import cr.ac.una.inforenta.negocio.LogicaVivienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MostrarViviendaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        //indicador de las accion a realizar
        int indicador = Integer.parseInt(request.getParameter("indicador"));
        
        // variables globales
        LogicaVivienda lvivienda;
        LogicaDescripcion ldescripcion;
        RequestDispatcher despachador;
        LinkedList<Vivienda> listaVivienda;
        LinkedList<Descripcion> listaDescripcion;
        
        switch (indicador) {
            
            case 1:     // indicador desde el index para cargar todas las viviendas y enviarlas al jsp
                
                lvivienda = new LogicaVivienda();
                
                despachador = request.getRequestDispatcher("MostrarVivienda.jsp");
                listaVivienda = lvivienda.getViviendas();
                request.setAttribute("listaViviendas", listaVivienda);
                despachador.forward(request, response);
                lvivienda.prueba();
                
                break;
                
            case 2:     // indicador del jsp que recive el idVivienda para mostrar la descripcion de la vivienda
                
                int idVivienda = Integer.parseInt(request.getParameter("idVivienda"));
                ldescripcion = new LogicaDescripcion();
                despachador = request.getRequestDispatcher("MostrarDescripcion.jsp");
                listaDescripcion = ldescripcion.getDescripcion(idVivienda);
                request.setAttribute("listaDescripcion", listaDescripcion);
                despachador.forward(request, response);
                
                break;
                
            case 3:
                break;
            default:
                break;
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
