/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Contrato;
import cr.ac.una.inforenta.negocio.LogicaContrato;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jacqueline
 */
public class EliminarContratoServlet extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        int cedula=Integer.parseInt(request.getParameter("ced"));
        //Obtiene datos del .jsp
        int idContrato = Integer.parseInt(request.getParameter("idContrato"));
                
        LogicaContrato lContrato= new LogicaContrato();
        
        String ruta = "";
        try {
            try{
                lContrato.eliminarContrato(idContrato);
                
                RequestDispatcher despachador= request.getRequestDispatcher("Mostrar_Contratos.jsp");
                LinkedList<Contrato> lista= lContrato.getContratos(cedula);
                request.setAttribute("listaContratos", lista);
                despachador.forward(request, response);  
                
            }catch(ClassNotFoundException ex){
                 Logger.getLogger(EliminarContratoServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EliminarContratoServlet.class.getName()).log(Level.SEVERE, null, ex);
            //ruta= "no_exito.jsp";
        }
    
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
