
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Vivienda;
import cr.ac.una.inforenta.negocio.LogicaVivienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EliminarViviendaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        int indiceEliminar = Integer.parseInt(request.getParameter("indiceEliminar"));
        
        // variables globales
        LogicaVivienda lvivienda;
        RequestDispatcher despachador;
        
        switch(indiceEliminar){
            
            case 1:     // indece del index a eliminar primero se cargan las viviendas para mandar al jsp
                
                lvivienda = new LogicaVivienda();
        
                despachador = request.getRequestDispatcher("EliminarVivienda.jsp");
                LinkedList<Vivienda> lista = lvivienda.getViviendas();
                request.setAttribute("listaViviendas", lista);
                despachador.forward(request, response);
                lvivienda.prueba();
                
                break;
                
            case 2:     // indice del jsp a que recive el id de vivienda a eliminar
                
                int idVivienda = Integer.parseInt(request.getParameter("idVivienda"));
        
                lvivienda = new LogicaVivienda();
                String ruta = "MostrarVivienda.jsp";

                try{
                    try{    
                        lvivienda.eliminarVivienda(idVivienda);

                    } catch (ClassNotFoundException ex1) {
                        ruta = "no_exito.jsp";
                        //request.getSession().setAttribute("nombre1", request.getParameter("La editorial contiene libros!!"));
                    }          
                } catch (SQLException ex1) {
                    Logger.getLogger(EliminarViviendaServlet.class.getName()).log(Level.SEVERE, null, ex1);
                    ruta = "no_exito.jsp";
                }
                despachador = request.getRequestDispatcher(ruta);
                despachador.forward(request, response);
                
                break;
                     
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
