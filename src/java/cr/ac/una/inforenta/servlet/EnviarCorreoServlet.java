/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.datos.DataLogin;
import cr.ac.una.inforenta.dominio.Login;
import cr.ac.una.inforenta.negocio.EnviarCorreo;
import cr.ac.una.inforenta.negocio.LogicaLogin;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EnviarCorreoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods..
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
         int cedula = Integer.parseInt(request.getParameter("ncedula"));
         DataLogin dLogin = new DataLogin();
         String[] datos = dLogin.recuperarContraseña(cedula);
         String ruta = ""; 
         
         if(datos[0]!= null){//Si esxisten datos
             datos[2]=  DigestUtils.md5Hex(datos[2]);//Sifra la clave
             EnviarCorreo correo = new EnviarCorreo(datos[1],datos[2]);
             correo.enviar();//La envia por gmai
               HttpSession session = request.getSession();//Crea una session para la validacion
                session.setMaxInactiveInterval(60);
                session.setAttribute("user", datos[0]);
                session.setAttribute("pass", datos[2]);
             ruta = "Recuperar_Cuenta.jsp";
         }else{
             request.setAttribute("err", "userinvalido");
             ruta = "Login.jsp";
         }
        RequestDispatcher despachador = request.getRequestDispatcher(ruta);
        despachador.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
