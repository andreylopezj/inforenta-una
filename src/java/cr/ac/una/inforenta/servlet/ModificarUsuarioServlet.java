/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.dominio.Usuario;
import cr.ac.una.inforenta.negocio.LogicaUsuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ModificarUsuarioServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        //Obtiene datos del .jsp
        int cedula = Integer.parseInt(request.getParameter("cedula"));
        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
         int telefono = Integer.parseInt(request.getParameter("telefono"));
        String direccion = request.getParameter("direccion");
        String correo = request.getParameter("correo");
        int tipoUsario = Integer.parseInt( request.getParameter("tipoUsuario"));
        Usuario usuario= new Usuario(cedula,nombre ,apellidos, telefono, direccion, correo,tipoUsario);
        System.out.println(usuario.toString());
        
        LogicaUsuario lUsuario= new LogicaUsuario();
        
        String ruta = "exito.jsp";
        try {
            try{
                lUsuario.modificarUsuario(usuario);//Modifica datos en tb_usuarios
                request.getSession().setAttribute("usuario", usuario);
                ruta = "Perfil_Usuario.jsp";
            }catch(ClassNotFoundException ex){
                 Logger.getLogger(ModificarUsuarioServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ModificarUsuarioServlet.class.getName()).log(Level.SEVERE, null, ex);
            ruta= "Perfil_Usuario.jsp";
        }
        
        RequestDispatcher despachador= request.getRequestDispatcher(ruta);
        despachador.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
