/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.servlet;

import cr.ac.una.inforenta.datos.DataFactura;
import cr.ac.una.inforenta.dominio.Factura;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DELL
 */
@WebServlet(name = "CRUDFacturaServlet", urlPatterns = {"/crudfactura"})
public class InsertarFacturaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        String nom= request.getParameter("nom");
        int idFactura= Integer.parseInt(request.getParameter("idFactura"));
        int idContrato= Integer.parseInt(request.getParameter("idContrato"));
        String nombreArrendador= request.getParameter("nombreArrendador");
        String nombreInquilino= request.getParameter("nombreInquilino");
        int monto= Integer.parseInt(request.getParameter("monto"));
        String descripcion= request.getParameter("descripcion");
        String fecha= request.getParameter("fecha");
        String modalidadPago= request.getParameter("modalidadPago");
        int montoTotal= monto;
        
        DataFactura dFactura= new DataFactura();
        Factura factura= new Factura(idFactura, idContrato, nombreArrendador, nombreInquilino, monto, descripcion, fecha, modalidadPago, montoTotal);
        LinkedList<Factura> dimension = dFactura.ultimoId();
        factura.setIdFactura(dimension.size()+1);
        try {
            try{
                dFactura.insertar(factura);//Registra en tb_facturas
                
                
            }catch(ClassNotFoundException ex){
                 Logger.getLogger(InsertarUsuarioServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InsertarUsuarioServlet.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
        RequestDispatcher despachador= request.getRequestDispatcher("CRUD_factura.jsp");
        LinkedList<Factura> lista= dFactura.getFacturas(nom);
        request.setAttribute("listaFacturas", lista);
        despachador.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
