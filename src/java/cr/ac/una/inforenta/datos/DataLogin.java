/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.datos;

import cr.ac.una.inforenta.dominio.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *
 * @author andre
 */
public class DataLogin extends BaseDatos{
    
    private static Connection conexion;

    public DataLogin() {
    }
    
    /* Funcion que Inserta un Login en tb_Login
    Atributos: Login
    Return: boolean
    */
    public boolean insertar(Login login) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        int id = getUltimoId()+1;
        String query = "call insertar_login ('"+id+"', '"+login.getCedula()+"', '"+login.getContrasena()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
    /* Funcion que Optiene ultimo id que se registro en tb_login
    Atributos: none
    Return: int
    */
    private int getUltimoId(){
       int ultimoId=0;
       
       String query= "CALL optener_ultimo_idUsuario()";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();    
            result.next();
            ultimoId = result.getInt("id"); 
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return ultimoId;
    }
    
    /* Funcion que Modifica un Login en tb_Login
    Atributos: Login
    Return: boolean
    */
    public boolean modificarContrasena(Login login) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call modificar_login('"+login.getCedula()+"','"+login.getContrasena()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
    /* Funcion que Optiene un Login de tb_Login
    Atributos: int
    Return: Login
    */
    public Login getLogin(int cedula){
       Login login = new Login();
       
       String query= "CALL mostrar_login('"+cedula+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();            
            result.next();
            login = new Login();
            login.setCedula(result.getInt("cedula"));
            login.setContrasena(result.getString("contrasena"));               
            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return login;
    }
  
    /* Funcion que elimina un Login en tb_Login
    Atributos: int
    Return: boolean
    */
    public boolean eliminarLogin(int cedula) throws ClassNotFoundException, SQLException{
        boolean elimino = false;
        String query = "CALL eliminar_login('"+cedula+"')";
        
        conexion = this.getConnection();
        if(conexion != null){
            System.out.println("exito estudiante"); 
        }
        PreparedStatement prepare = conexion.prepareStatement(query);
        elimino = prepare.execute();
        prepare.close();
        conexion.close();
        
        return elimino;
    }
    
    /* Funcion que verefica el Inicio de Secion en tb_login
    Atributos: int, String
    Return: boolean
    */
    public Login iniciarSecion(int cedula, String contrasena){
       Login login = new Login();
       String query= "CALL mostrar_login('"+cedula+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            
            result.next();
            login.setCedula(result.getInt("cedula"));
            login.setContrasena(result.getString("contrasena"));
            prepare.close();
            
            conexion.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return login;
    }
    
    public String[] recuperarContraseña(int cedula){
        String[] datos = new String[3];
        String query= "CALL recuperar_contrasena('"+cedula+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            
            result.next();
            datos[0] = result.getInt("cedula")+"";
            datos[1] = result.getString("correo");
            datos[2] = result.getString("contrasena"); 
            prepare.close();
            
            conexion.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        return datos;
    }
    
    
}
