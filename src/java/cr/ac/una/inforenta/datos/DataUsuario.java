package cr.ac.una.inforenta.datos;

import cr.ac.una.inforenta.dominio.Usuario;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class DataUsuario extends BaseDatos{
    
    private static Connection conexion;
    
    /* Funcion que Insertar un Arrendador/Inquilino en tb_usuarios
    Parametros: Usuario
    Return: boolean
    */
    public boolean insertar(Usuario usuario) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
                
        String query = "call insertar_usuario ('"+usuario.getCedula()+"', '"+usuario.getNombre()+"', '"+usuario.getApellidos()+"', '"+
                usuario.getTelefono()+"', '"+usuario.getDireccion()+"', '"+usuario.getCorreo()+"',"
                + " '"+usuario.getTipoUsuario()+"', '"+1+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
   
    /* Funcion que Modifica un Usuario en tb_usuario
    Parametros: Usuario
    Return: boolean
    */
    public boolean modificarUsuario(Usuario usuario) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        System.out.println(usuario.toString());
        String query = "call modificar_usuario('"+usuario.getCedula()+"','"+usuario.getNombre()+"','"+usuario.getApellidos()+
                "','"+usuario.getTelefono()+"','"+usuario.getDireccion()+"','"+usuario.getCorreo()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
  
    public boolean eliminarUsuario(int cedula) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call eliminar_usuario('"+cedula+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
   
   /* Funcion que optiene una lista de Usuarios de tb_Usuario segun el tipo
    Atributos: int
    Return: LinkedList<Usuario>
    */
   public LinkedList<Usuario> getUsuarios(int tipo) {
        LinkedList<Usuario> lista= new LinkedList();
        String query= "call mostrar_usuarios('"+tipo+"')";
        
        try {
           conexion= this.getConnection();
           PreparedStatement prepare= conexion.prepareCall(query);
           ResultSet result= prepare.executeQuery();//recupera una lista simple
           Usuario usuario = null;
           
           while(result.next()){
               usuario = new Usuario();
               usuario.setCedula(result.getInt("cedula"));
               usuario.setNombre(result.getString("nombre"));
               usuario.setApellidos(result.getString("apellidos"));
               usuario.setTelefono(result.getInt("telefono"));
               usuario.setDireccion(result.getString("direccion"));
               usuario.setCorreo(result.getString("correo"));
               
               lista.add(usuario);
           }
           prepare.close();
           conexion.close();
     
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }catch(SQLException ex){
            Logger.getLogger(DataUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    /* Funcion que Optiene un Usuario de tb_usuarios
    Atributos: int
    Return: Usuario
    */
    public Usuario getUsuario(int cedula) {
        Usuario usuario = new Usuario();
       
       String query= "call mostrar_usuario('"+cedula+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();            
            result.next();
            usuario = new Usuario();
            usuario.setCedula(Integer.parseInt(result.getString("cedula")));
            usuario.setNombre(result.getString("nombre"));
            usuario.setApellidos(result.getString("apellidos"));
            usuario.setTelefono(result.getInt("telefono"));
            usuario.setDireccion(result.getString("direccion"));
            usuario.setCorreo(result.getString("correo"));
            usuario.setTipoUsuario(result.getInt("tipoUsuario"));
            System.out.println(usuario.getCedula());
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return usuario;
    }
   
    /////////////////// Datos usuario
    public LinkedList<Usuario> mostrarInquilino() {
        LinkedList<Usuario> lista= new LinkedList();
        //String query= "SELECT * FROM tb_editoriales";
        String query= "call mostrar_inquilinos()";
        
           
        try {
           conexion= this.getConnection();
           PreparedStatement prepare= conexion.prepareCall(query);
           ResultSet result= prepare.executeQuery();//recupera una lista simple
           Usuario usuario = null;
           
           while(result.next()){
               usuario = new Usuario();
               usuario.setCedula(result.getInt("cedula"));
               usuario.setNombre(result.getString("nombre")); 
               usuario.setApellidos(result.getString("apellidos")); 
               lista.add(usuario);
           }
           prepare.close();
           conexion.close();
     
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }catch(SQLException ex){
            Logger.getLogger(DataUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    
    
    
    
    
}
