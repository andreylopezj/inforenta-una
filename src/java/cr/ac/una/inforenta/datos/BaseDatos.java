package cr.ac.una.inforenta.datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseDatos {
    
    private static Connection conexion;;
   /* private static final String user="root";
    private static  final String passw="";
    private static final String server= "localhost";*/
    
    private static final String user="inforenta";   //inforenta
    private static  final String passw="e56lsa2Dotes8"; //e56lsa2Dotes8
    private static final String server= "portalinfofc.com"; //portalinfofc.com
    
    
    private static final String url="jdbc:mysql://"+server+":3306/inforenta";    //inforenta
    
    /*Funcion que Establece una conexion con la Base de Datos
    Parametros: none
    Return: Connection
    */
    public Connection getConnection() throws ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conexion= DriverManager.getConnection(url, user, passw);
            System.out.println("Conecto a la base de datos");
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
        return this.conexion;
    }
}
