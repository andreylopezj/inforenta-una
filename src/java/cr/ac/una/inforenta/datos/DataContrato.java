
package cr.ac.una.inforenta.datos;

import cr.ac.una.inforenta.dominio.Contrato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataContrato extends BaseDatos{
    
    private static Connection conexion;
    
    public boolean insertarContratos(Contrato contrato) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
                
        String query = "INSERT INTO tb_contratos (idContrato,idArrendador,nombreArrendador,telefono,datos_inquilino,idVivienda,fecha,monto,cuentaBancaria,plazoValidezContrato,estado) VALUES(?,?,?,?,?,?,?,?,?,?,?);";
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        prepare.setInt(1,contrato.getIdContrato());
        prepare.setInt(2,contrato.getIdArrendador());
        prepare.setString(3,contrato.getNombreArrendador());
        prepare.setInt(4,contrato.getTelefono());
        prepare.setString(5,contrato.getDatos_inquilino());
        prepare.setInt(6,contrato.getIdVivienda());
        prepare.setString(7,contrato.getFecha());
        prepare.setFloat(8,contrato.getMonto());
        prepare.setString(9,contrato.getCuentaBancaria());
        prepare.setString(10,contrato.getPlazoValidezContrato());
        prepare.setString(11,"1");

        inserto= prepare.execute();
        prepare.close();
        conexion.close(); 
        return inserto;
    }
    
     public int ultimoIdContrato() throws SQLException, ClassNotFoundException{
        int idContrato=0;
        String query= "CALL ultimoContrato";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            result.next();
            idContrato=result.getInt("idContrato");            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataContrato.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataContrato.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return idContrato;        
    }
     
     public LinkedList<Contrato> getContratos(int cedula) {
        LinkedList<Contrato> lista= new LinkedList();
        String query= "call mostrar_contratos('"+cedula+"')";
        
        try {
           conexion= this.getConnection();
           PreparedStatement prepare= conexion.prepareCall(query);
           ResultSet result= prepare.executeQuery();//recupera una lista simple
           Contrato contrato = null;
          
           while(result.next()){
               contrato = new Contrato();
               contrato.setIdContrato(result.getInt("idContrato"));
               contrato.setIdArrendador(result.getInt("idArrendador"));
               contrato.setNombreArrendador(result.getString("nombreArrendador"));
               contrato.setTelefono(result.getInt("telefono"));
               contrato.setDatos_inquilino(result.getString("datos_inquilino"));
               contrato.setIdVivienda(result.getInt("idVivienda"));
               contrato.setFecha(result.getString("fecha"));
               contrato.setMonto(result.getFloat("monto"));
               contrato.setCuentaBancaria(result.getString("cuentaBancaria"));
               contrato.setPlazoValidezContrato(result.getString("plazoValidezContrato"));               
               lista.add(contrato);
           }
           prepare.close();
           conexion.close();
     
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataContrato.class.getName()).log(Level.SEVERE, null, ex);
        }catch(SQLException ex){
            Logger.getLogger(DataContrato.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
     
     public boolean eliminarContrato(int idContrato) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call eliminar_contrato('"+idContrato+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }

    public boolean modificarContrato(Contrato contrato) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call modificar_contrato('"+contrato.getIdContrato()+"','"+contrato.getTelefono()+"','"+contrato.getMonto()+"','"+contrato.getPlazoValidezContrato()+
                "','"+contrato.getCuentaBancaria()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
}
