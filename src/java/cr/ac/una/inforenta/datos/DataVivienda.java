
package cr.ac.una.inforenta.datos;

import cr.ac.una.inforenta.dominio.Vivienda;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class DataVivienda extends BaseDatos{
    
    private static Connection conexion;
    
    /* Funcion que inserta una Vivienda en tb_viviendas
    Parametros: Vivienda
    Return: boolean
    */
    public boolean insertarVivienda(Vivienda vivienda)throws SQLException, ClassNotFoundException{
        boolean inserto = false;
        String query ="CALL insertar_vivienda ('"+vivienda.getIdVivienda()+"', '"+vivienda.getIdDescripcion()+"', '"+vivienda.getDireccion()+"', '"+
                vivienda.getNiceAgua()+"', '"+vivienda.getNiceElectricidad()+"', '"+vivienda.getPrecio()+"', '"+vivienda.getImagen1()+
                "', '"+vivienda.getImagen2()+"', '"+vivienda.getImagen3()+"', '"+vivienda.getImagen4()+"', '"+vivienda.getImagen5()+
                "', '"+1+"')";
        
        conexion= this.getConnection();
        
        if(conexion != null){
            System.out.println("exito inserto vivienda"); 
        }
        PreparedStatement prepare = conexion.prepareCall(query);
        inserto = prepare.execute();
        prepare.close();
        conexion.close();
        
        return inserto;
    }
    
    
    // metodo que obtiene una lista con los datos de las viviendas y las descripciones     
    public String vivienda_descripcion(){
        String datos ="";
        String query= "CALL vivienda_descripcion()";
        
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            
            while(result.next()){
                datos += result.getInt("idVivienda")+"-"+result.getString("direccion")+"-"+result.getInt("niceAgua")+"-"+
                        result.getInt("niceLuz")+"-"+result.getInt("precio")+"-"+result.getString("imagen1")+"-"+
                        result.getString("imagen2")+"-"+result.getString("imagen3")+"-"+result.getString("imagen4")+"-"+
                        result.getString("imagen5")+"-"+result.getInt("internet")+"-"+result.getInt("cable")+"-"+
                        result.getInt("mascotas")+"-"+result.getInt("garage")+"-"+result.getInt("zonaVerde")+"-"+
                        result.getInt("cuartoPila")+"-"+result.getString("dimension")+"-"+result.getInt("ninos")+"-"+
                        result.getInt("cuartos")+"-"+result.getString("tipoVivienda")+"-"+result.getInt("amueblado")+"-"+
                        result.getInt("banos")+"-"+result.getInt("sistemaSeguridad")+"-"+result.getInt("visitas")+"#";                
            }
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return datos;
    }
    
    
    // metodo que me muestra los detalles de una vivienda en especifico
     public String ver_detalleVivienda(int id){
        String datos ="";
        String query= "CALL ver_detalle_vivienda('"+id+"')";
        
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            
            while(result.next()){
                datos += result.getInt("idVivienda")+"-"+result.getString("direccion")+"-"+result.getInt("niceAgua")+"-"+
                        result.getInt("niceLuz")+"-"+result.getInt("precio")+"-"+result.getString("imagen1")+"-"+
                        result.getString("imagen2")+"-"+result.getString("imagen3")+"-"+result.getString("imagen4")+"-"+
                        result.getString("imagen5")+"-"+result.getInt("internet")+"-"+result.getInt("cable")+"-"+
                        result.getInt("mascotas")+"-"+result.getInt("garage")+"-"+result.getInt("zonaVerde")+"-"+
                        result.getInt("cuartoPila")+"-"+result.getString("dimension")+"-"+result.getInt("ninos")+"-"+
                        result.getInt("cuartos")+"-"+result.getString("tipoVivienda")+"-"+result.getInt("amueblado")+"-"+
                        result.getInt("banos")+"-"+result.getInt("sistemaSeguridad")+"-"+result.getInt("visitas")+"#";                
            }
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return datos;
    }
    
    // metodo para obtener toda la lista de viviendas de la BD
    /* Funcion que optiene una Lista de Viviendas de tb_viviendas
    Parametros: none;
    Return: LinkedList<Vivienda>
    */
    public LinkedList<Vivienda> getViviendas(){
        
       LinkedList<Vivienda> lista = new LinkedList();
       String query= "CALL mostrar_viviendas()";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            Vivienda vivienda = null;
            
            while(result.next()){
                vivienda = new Vivienda();
                vivienda.setIdVivienda(result.getInt("idVivienda"));
                vivienda.setDireccion(result.getString("direccion"));
                vivienda.setNiceAgua(result.getInt("niceAgua"));
                vivienda.setNiceElectricidad(result.getInt("niceLuz"));
                vivienda.setPrecio(result.getInt("precio"));
                vivienda.setImagen1(result.getString("imagen1"));
                vivienda.setImagen2(result.getString("imagen2"));
                vivienda.setImagen3(result.getString("imagen3"));
                vivienda.setImagen4(result.getString("imagen4"));
                vivienda.setImagen5(result.getString("imagen5"));
                vivienda.setIdDescripcion(result.getInt("idDescripcion"));
                lista.add(vivienda);
            }
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return lista;
    }
    
    /* Funcion que Obtiene una Vivienda de tb_viviendas
    Parametros: int
    Return: Vivienda
    */
    public Vivienda getVivienda(int idvivienda)throws SQLException, ClassNotFoundException{
       Vivienda vivienda = new Vivienda();
       
       String query= "call mostrar_vivienda('"+idvivienda+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();            
            
            vivienda = new Vivienda();
            vivienda.setDireccion(result.getString("direccion"));
            vivienda.setNiceAgua(result.getInt("niceAgua"));
            vivienda.setNiceElectricidad(result.getInt("niceLuz"));
            vivienda.setPrecio(result.getInt("precio"));
            vivienda.setImagen1(result.getString("imagen1"));
            vivienda.setImagen2(result.getString("imagen2"));
            vivienda.setImagen3(result.getString("imagen3"));
            vivienda.setImagen4(result.getString("imagen4"));
            vivienda.setImagen5(result.getString("imagen5"));
            vivienda.setIdDescripcion(result.getInt("idDescripcion"));                
            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return vivienda;
    }
    
    /* Funcion que elimina una Vivienda de tb_viviendas
    Parametros: int
    Return: boolean
    */
    public boolean eliminarVivienda(int id) throws ClassNotFoundException, SQLException{
        boolean elimino = false;
        String query = "CALL eliminar_vivienda(?)";
        String query2="UPDATE tb_viviendas SET estado = 0 WHERE tb_viviendas.idVivienda=(?)";
        
        conexion = this.getConnection();
        if(conexion != null){
            System.out.println("exito eliminacionVivienda"); 
        }
        PreparedStatement prepare = conexion.prepareCall(query);
        prepare.setInt(1, id);
        
        elimino = prepare.execute();
        prepare.close();
        conexion.close();
        
        return elimino;
    }
    
    /*Funcion que Modifica una Vivienda en tb_viviendas
    Parametros: Vivienda
    Return: boolean
    */
    public boolean actualizarVivienda(Vivienda vivienda) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call modificar_vivienda('"+vivienda.getIdVivienda()+"','"+vivienda.getDireccion()+"','"+
                vivienda.getNiceElectricidad()+"','"+vivienda.getNiceAgua()+"','"+vivienda.getPrecio()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito actualizacionVivienda");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
    /*Funcion que obtiene el ultimo idDescripcion de tb_Vivienda
    Parametros: none
    Return: int
    */
    public int ultimoIdVivienda() throws SQLException, ClassNotFoundException{
        int idVivienda=0;
        String query= "CALL ultimaVivienda";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            result.next();
            idVivienda=result.getInt("idVivienda");            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return idVivienda;        
    }
    
    
    // metodos para el filtrado de las viviendas en el index
    
    // metodo que filtra por el precio de las viviendas
    public LinkedList<Vivienda> getFiltro_Precio(int precio){
        
       LinkedList<Vivienda> lista = new LinkedList();
       String query= "CALL filtrar_precio('"+precio+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            Vivienda vivienda = null;
            
            while(result.next()){
                vivienda = new Vivienda();
                vivienda.setIdVivienda(result.getInt("idVivienda"));
                vivienda.setDireccion(result.getString("direccion"));
                vivienda.setNiceAgua(result.getInt("niceAgua"));
                vivienda.setNiceElectricidad(result.getInt("niceLuz"));
                vivienda.setPrecio(result.getInt("precio"));
                vivienda.setImagen1(result.getString("imagen1"));
                vivienda.setImagen2(result.getString("imagen2"));
                vivienda.setImagen3(result.getString("imagen3"));
                vivienda.setImagen4(result.getString("imagen4"));
                vivienda.setImagen5(result.getString("imagen5"));
                vivienda.setIdDescripcion(result.getInt("idDescripcion"));
                lista.add(vivienda);
            }
            System.out.println("\t\tfiltro por precio");
            for(Vivienda v:lista){
                System.out.println(v);
            }
            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return lista;
    }
    
    
    // metodo que filtra por la cantidad de cuertos de las viviendas
    public LinkedList<Vivienda> getFiltro_Cuartos(int cuartos){
        
       LinkedList<Vivienda> lista = new LinkedList();
       String query= "CALL filtrar_cantCuartos('"+cuartos+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            Vivienda vivienda = null;
            
            while(result.next()){
                vivienda = new Vivienda();
                vivienda.setIdVivienda(result.getInt("idVivienda"));
                vivienda.setDireccion(result.getString("direccion"));
                vivienda.setNiceAgua(result.getInt("niceAgua"));
                vivienda.setNiceElectricidad(result.getInt("niceLuz"));
                vivienda.setPrecio(result.getInt("precio"));
                vivienda.setImagen1(result.getString("imagen1"));
                vivienda.setImagen2(result.getString("imagen2"));
                vivienda.setImagen3(result.getString("imagen3"));
                vivienda.setImagen4(result.getString("imagen4"));
                vivienda.setImagen5(result.getString("imagen5"));
                vivienda.setIdDescripcion(result.getInt("idDescripcion"));
                lista.add(vivienda);
            }
            
            System.out.println("\t\tfiltro por cuartos");
            for(Vivienda v:lista){
                System.out.println(v);
            }
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return lista;
    }
    
    
    // metodo que filtra por el precio y la cantidad de cuartos de las viviendas
    public LinkedList<Vivienda> getFiltro_CuartosPrecio(int cuartos, int precio){
        
       LinkedList<Vivienda> lista = new LinkedList();
       String query= "CALL filtrar_cantCuartosPrecio('"+cuartos+"', '"+precio+"')";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            Vivienda vivienda = null;
            
            while(result.next()){
                vivienda = new Vivienda();
                vivienda.setIdVivienda(result.getInt("idVivienda"));
                vivienda.setDireccion(result.getString("direccion"));
                vivienda.setNiceAgua(result.getInt("niceAgua"));
                vivienda.setNiceElectricidad(result.getInt("niceLuz"));
                vivienda.setPrecio(result.getInt("precio"));
                vivienda.setImagen1(result.getString("imagen1"));
                vivienda.setImagen2(result.getString("imagen2"));
                vivienda.setImagen3(result.getString("imagen3"));
                vivienda.setImagen4(result.getString("imagen4"));
                vivienda.setImagen5(result.getString("imagen5"));
                vivienda.setIdDescripcion(result.getInt("idDescripcion"));
                lista.add(vivienda);
            }
            
            System.out.println("\t\tfiltro por precio y cuarto");
            for(Vivienda v:lista){
                System.out.println(v);
            }
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataVivienda.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return lista;
    }
    
}
