/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.datos;

import cr.ac.una.inforenta.dominio.Factura;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class DataFactura extends BaseDatos{
     
    private static Connection conexion;
     
    public boolean insertar(Factura factura) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
                
        String query = "call insertar_factura ('"+factura.getIdFactura()+"', '"+factura.getIdContrato()+"', '"+factura.getNombreArrendador()+"', '"+
                factura.getNombreInquilino()+"', '"+factura.getMonto()+"','"+factura.getDescripcion()+"','"+factura.getFecha()+"', '"+factura.getModalidadPago()+"', '"+factura.getMontoTotal()+"', '"+1+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
    /* Funcion que Modifica una Factura en tb_facturas
    Parametros: Factura
    Return: boolean
    */
     public boolean modificar(Factura factura) throws SQLException, ClassNotFoundException{
        boolean modifico  = false;
        String query = "UPDATE tb_facturas SET nombreArrendador=?, nombreInquilino=?, monto=?,descripcion=?, modalidadPago=?, montoTotal=? WHERE idFactura="+factura.getIdFactura();                                             
        
        conexion = this.getConnection();
        if(conexion!=null){
            System.out.println("modificacion conecta");
        }
        PreparedStatement prepare = conexion.prepareStatement(query);
        prepare.setString(1,factura.getNombreArrendador());
        prepare.setString(2,factura.getNombreInquilino());
        prepare.setInt(3, factura.getMonto());
        prepare.setString(4,factura.getDescripcion());
        prepare.setString(5,factura.getModalidadPago());
        prepare.setInt(6,factura.getMontoTotal());
        
        modifico = prepare.execute();
        prepare.close();    
        
        conexion.close();
        return modifico;
    }
    
     /* Funcion que elimina una Factura en tb_facturas
    Atributos: int
    Return: boolean
    */
    public boolean eliminarFactura(int idFactura) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call eliminar_factura('"+idFactura+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
     /* Funcion que optiene una lista de Facturas de tb_facturas segun el tipo
    Atributos: int
    Return: LinkedList<Factura>
    */
    public LinkedList<Factura> getFacturas(String nombreArrendador) {
        LinkedList<Factura> lista= new LinkedList();
        String query= "call mostrar_factura_individual('"+nombreArrendador+"')";
        
        try {
           conexion= this.getConnection();
           PreparedStatement prepare= conexion.prepareCall(query);
           ResultSet result= prepare.executeQuery();//recupera una lista simple
           Factura factura = null;
           
           while(result.next()){
               factura = new Factura();
               factura.setIdFactura(result.getInt("idFactura"));
               factura.setIdContrato(result.getInt("idContrato"));
               factura.setNombreArrendador(result.getString("nombreArrendador"));
               factura.setNombreInquilino(result.getString("nombreInquilino"));
               factura.setMonto(result.getInt("monto"));
               factura.setDescripcion(result.getString("descripcion"));
               factura.setFecha(result.getString("fecha"));
               factura.setModalidadPago(result.getString("modalidadPago"));
               factura.setMontoTotal(result.getInt("montoTotal"));
               lista.add(factura);
           }
           prepare.close();
           conexion.close();
     
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataFactura.class.getName()).log(Level.SEVERE, null, ex);
        }catch(SQLException ex){
            Logger.getLogger(DataFactura.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public LinkedList<Factura> ultimoId() {
          LinkedList<Factura> lista = new LinkedList<Factura>();
        String query = "SELECT * FROM tb_facturas";
         try {
             conexion = this.getConnection();
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(DataFactura.class.getName()).log(Level.SEVERE, null, ex);
         }
        PreparedStatement prepare;
        try {
            prepare = conexion.prepareCall(query);
            ResultSet result = prepare.executeQuery();
            Factura factura = null;
            while(result.next()){
                factura = new Factura();
                factura.setIdFactura(result.getInt("idFactura"));
                factura.setIdContrato(result.getInt("idContrato"));
                factura.setNombreArrendador(result.getString("nombreArrendador"));
                factura.setNombreInquilino(result.getString("nombreInquilino"));
                factura.setMonto(result.getInt("monto"));
                factura.setDescripcion(result.getString("descripcion"));
                factura.setFecha(result.getDate("fecha")+"");
                factura.setModalidadPago(result.getString("modalidadPago"));
                factura.setMontoTotal(result.getInt("montoTotal"));
                lista.add(factura);
            }
            prepare.close();
            conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataFactura.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
     public Factura mostrarFactura(int id) throws SQLException{
        Factura factura = null;
        
        String query = "SELECT * FROM tb_facturas WHERE tb_facturas.estado != 0 AND tb_facturas.idFactura="+id;
         try {
             conexion = this.getConnection();
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(DataFactura.class.getName()).log(Level.SEVERE, null, ex);
         }
        PreparedStatement prepare;
        
            prepare = conexion.prepareCall(query);
            ResultSet result = prepare.executeQuery();
                result.next();
                factura = new Factura();
                factura.setIdFactura(result.getInt("idFactura"));
                factura.setIdContrato(result.getInt("idContrato"));
                factura.setNombreArrendador(result.getString("nombreArrendador"));
                factura.setNombreInquilino(result.getString("nombreInquilino"));
                factura.setMonto(result.getInt("monto"));
                factura.setDescripcion(result.getString("descripcion"));
                factura.setFecha(result.getDate("fecha")+"");
                factura.setModalidadPago(result.getString("modalidadPago"));
                factura.setMontoTotal(result.getInt("montoTotal"));
            
            prepare.close();
            conexion.close();
        
        
        return factura;
    }
     
    /*
    
    /* Funcion que Modifica un Usuario en tb_usuario
    Parametros: Usuario
    Return: boolean
    
    public boolean modificarUsuario(Usuario usuario) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call modificar_usuario('"+usuario.getCedula()+"','"+usuario.getTelefono()+
                "','"+usuario.getDireccion()+"','"+usuario.getCorreo()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
    /* Funcion que elimina un Usuario en tb_Usuario
    Atributos: int
    Return: boolean
    
    public boolean eliminarUsuario(int cedula) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        String query = "call eliminar_usuario('"+cedula+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }*/


}
