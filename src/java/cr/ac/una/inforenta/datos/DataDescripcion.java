/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.datos;

import cr.ac.una.inforenta.dominio.Descripcion;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author andre
 */
public class DataDescripcion extends BaseDatos{
    
    private static Connection conexion;
    
    /* Función que Inserta una Vivienda en la tb_descripcion
    Parametros: Descipcion;
    Return: boolean;
    */
    public boolean insertarDescripcionVivienda(Descripcion descripcion)throws SQLException, ClassNotFoundException{
        boolean inserto = false;
        String query ="CALL insertar_descripcion('"+descripcion.getIdDescripcion()+"', '"+descripcion.getIdVivienda()+"', '"+descripcion.getInternet()+
                "', '"+descripcion.getCable()+"', '"+descripcion.getMascotas()+"', '"+descripcion.getGarage()+"', '"+descripcion.getZonaVerde()+"', '"+
                descripcion.getCuartoPila()+"', '"+descripcion.getDimensiones()+"', '"+descripcion.getNinos()+"', '"+descripcion.getCantCuartos()+"', '"+
                descripcion.getTipoVivienda()+"', '"+descripcion.getAmueblado()+"', '"+descripcion.getCantBanos()+"', '"+descripcion.getSistemaSeguridad()+
                "', '"+descripcion.getVisitas()+"')";
        
        conexion= this.getConnection();
        if(conexion != null) {
            System.out.println("exito inserto descripcion");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        
        return inserto;
    }
    
    /* Funcion que obtiene una Descipcion en tb_descripciones
    Parametros: int;
    Return: Descripcion;
    */
    public LinkedList<Descripcion> getDescripcion(int idVivienda){
        
       LinkedList<Descripcion> lista = new LinkedList();
       String query= "CALL mostrar_descripcion('"+idVivienda+"')";
       
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            Descripcion descripcion = null;
            
            while(result.next()){
                descripcion = new Descripcion();
                descripcion.setIdVivienda(result.getInt("idVivienda"));
                descripcion.setInternet(result.getInt("internet"));
                descripcion.setCable(result.getInt("cable"));
                descripcion.setMascotas(result.getInt("mascotas"));
                descripcion.setGarage(result.getInt("garage"));
                descripcion.setZonaVerde(result.getInt("zonaVerde"));
                descripcion.setCuartoPila(result.getInt("cuartoPila"));
                descripcion.setDimensiones(result.getString("dimension"));
                descripcion.setNinos(result.getInt("ninos"));
                descripcion.setCantCuartos(result.getInt("cuartos"));
                descripcion.setTipoVivienda(result.getString("tipoVivienda"));
                descripcion.setAmueblado(result.getInt("amueblado"));
                descripcion.setCantBanos(result.getInt("banos"));
                descripcion.setSistemaSeguridad(result.getInt("sistemaSeguridad"));
                descripcion.setVisitas(result.getInt("visitas"));
                lista.add(descripcion);
            }
            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataDescripcion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataDescripcion.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return lista;
    }
    
    /* Funcion que elimina una Descripcion en tb_descripciones
    Atributos: int
    Return: boolean
    */
    public boolean eliminarDescripcion(int idVivienda) throws ClassNotFoundException, SQLException{
        boolean elimino = false;
        String query = "CALL eliminar_descripcion('"+idVivienda+"')";
        
        conexion = this.getConnection();
        if(conexion != null){
            System.out.println("exito eliminandoDescripcion"); 
        }
        PreparedStatement prepare = conexion.prepareCall(query);
        elimino = prepare.execute();
        prepare.close();
        conexion.close();
        
        return elimino;
    }
    
    /* metodo para actualizar datos de vivienda
    Atributos: Descripcion
    Return: boolean
    */
    public boolean actualizarDescripcion(Descripcion descripcion) throws SQLException, ClassNotFoundException{
        boolean inserto=false;
        
        String query = "call modificar_descripcion('"+descripcion.getIdVivienda()+"','"+descripcion.getInternet()+"','"+
                descripcion.getCable()+"','"+descripcion.getMascotas()+"','"+descripcion.getGarage()+"','"+
                descripcion.getZonaVerde()+"','"+descripcion.getCuartoPila()+"','"+
                descripcion.getDimensiones()+"','"+descripcion.getNinos()+"','"+
                descripcion.getCantCuartos()+"','"+descripcion.getTipoVivienda()+"','"+
                descripcion.getAmueblado()+"','"+descripcion.getCantBanos()+"','"+
                descripcion.getSistemaSeguridad()+"','"+descripcion.getVisitas()+"')";
        
        conexion= this.getConnection();
        
        if(conexion!=null){
            System.out.println("Exito actualizandoDescripcion");
        }
        PreparedStatement prepare= conexion.prepareCall(query);
        inserto= prepare.execute();
        prepare.close();
        conexion.close();
        return inserto;
    }
    
    /*Funcion que obtiene el ultimo idVivienda de tb_viviendas
    Parametros: none
    Return: int
    */
    public int ultimoIdDescripcion() throws SQLException, ClassNotFoundException{
        int idDescripcion=0;
        String query= "CALL ultimaDescripcion";
        try {
            conexion = this.getConnection();
            PreparedStatement prepare = conexion.prepareCall(query);
            ResultSet result= prepare.executeQuery();
            result.next();
            idDescripcion=result.getInt("idDescripcion");
            
            prepare.close();
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataDescripcion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataDescripcion.class.getName()).log(Level.SEVERE, null, ex);
        }        
       return idDescripcion;        
    }
    
}
