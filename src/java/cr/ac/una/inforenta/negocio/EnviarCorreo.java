/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cr.ac.una.inforenta.negocio;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EnviarCorreo {
    
    Properties propiedad;
    Session sesion;
    private String correoEnvia;
    private String contrasena;
    private String receptor;
    private String asunto;
    private String mensaje;
    private String logo;
    
    public EnviarCorreo(String correoDestinatario, String clave){
        iniciarlizarConexion(correoDestinatario, clave);
    }
    
    public void iniciarlizarConexion(String correoDestinatario, String clave){
        propiedad = new Properties();
        propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
        propiedad.setProperty("mail.smtp.starttls.enable", "true");
        propiedad.setProperty("mail.smtp.port", "587");
        propiedad.setProperty("mail.smtp.auth", "true");
        
        sesion = Session.getDefaultInstance(propiedad);
        correoEnvia = "unainforentacr@gmail.com";
        contrasena = "123456ayj";
        receptor = correoDestinatario;
        asunto = "Recuperacion de contraseña";
        mensaje="Digite la siguiente clave para recuperar su cuenta\n\t\t\t"+clave;
        logo = "logo.png";
    }
    
    public boolean enviar(){
        boolean envio=false;
         try{
            MimeMessage mail = new MimeMessage(sesion);
            mail.setFrom(new InternetAddress (correoEnvia));
            mail.addRecipient(Message.RecipientType.TO, new InternetAddress (receptor));
            mail.setSubject(asunto);
            mail.setText(mensaje);
            
            Transport transportar = sesion.getTransport("smtp");
            transportar.connect(correoEnvia,contrasena);
            transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));          
            transportar.close();
            envio=true;
        }catch (AddressException ex) {
                Logger.getLogger(EnviarCorreo.class.getName()).log(Level.SEVERE, null, ex);
        }catch (MessagingException ex) {
                Logger.getLogger(EnviarCorreo.class.getName()).log(Level.SEVERE, null, ex);
            }
        return envio;
    }
          
        

}
