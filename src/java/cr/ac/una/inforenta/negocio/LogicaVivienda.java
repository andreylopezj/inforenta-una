/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.inforenta.negocio;

import cr.ac.una.inforenta.datos.DataVivienda;
import cr.ac.una.inforenta.dominio.Vivienda;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author andre
 */
public class LogicaVivienda {
    
    private static DataVivienda dVivienda;

    public LogicaVivienda() {
        dVivienda = new DataVivienda();
    }
    
    public void prueba(){
        System.out.println("esto es una prueba a ver si esto esta funcionando-----asda----a-d-ad-");
    }
    
    public boolean insertarVivenda(Vivienda vivienda) throws SQLException, ClassNotFoundException{
        return dVivienda.insertarVivienda(vivienda);
    }
    
    public static LinkedList<Vivienda> getViviendas(){
        return dVivienda.getViviendas();
    }
    
    public String vivienda_descripcion(){
        return dVivienda.vivienda_descripcion();
    }
    
    public String ver_detalleVivienda(int id){
        return dVivienda.ver_detalleVivienda(id);
    }
    
    public Vivienda getVivienda(int idVivienda) throws SQLException, ClassNotFoundException{
        return dVivienda.getVivienda(idVivienda);
    }
    
    public boolean  eliminarVivienda(int idVivienda) throws SQLException, ClassNotFoundException {
        return dVivienda.eliminarVivienda(idVivienda);
    }
    
    public boolean actualizarVivienda(Vivienda vivienda) throws SQLException, ClassNotFoundException{
        return dVivienda.actualizarVivienda(vivienda);
    }
    
    public int ultimoIdVivienda() throws SQLException, ClassNotFoundException{
        return dVivienda.ultimoIdVivienda();
    }
    
    // metodos para el filtrado de las viviendas en el index
    public LinkedList<Vivienda> getFiltro_Precio(int precio){
        return dVivienda.getFiltro_Precio(precio);
    }
    
    public LinkedList<Vivienda> getFiltro_Cuartos(int cuartos){
        return dVivienda.getFiltro_Cuartos(cuartos);
    }
    
   public LinkedList<Vivienda> getFiltro_CuartosPrecio(int cuartos, int precio){
        return dVivienda.getFiltro_CuartosPrecio(cuartos, precio);
    }
    
}
