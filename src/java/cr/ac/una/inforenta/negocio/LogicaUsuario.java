package cr.ac.una.inforenta.negocio;

import cr.ac.una.inforenta.datos.DataUsuario;
import cr.ac.una.inforenta.dominio.Usuario;
import java.sql.SQLException;
import java.util.LinkedList;

public class LogicaUsuario {
    
    private static DataUsuario dUsuario;

    public LogicaUsuario() {
        this.dUsuario= new DataUsuario();
    }
   
    public boolean insertar(Usuario usuario) throws SQLException, ClassNotFoundException{
        return dUsuario.insertar(usuario);
    }
    
    public LinkedList<Usuario> getUsuarios(int tipo) {
        return this.dUsuario.getUsuarios(tipo);
    }
   
    public Usuario getUsuario(int cedula) {
        return this.dUsuario.getUsuario(cedula);
    }
    
    public boolean modificarUsuario(Usuario usuario) throws SQLException, ClassNotFoundException{
        return dUsuario.modificarUsuario(usuario);
    }
    
    public boolean  eliminarUsuario(int cedula) throws SQLException, ClassNotFoundException {
        return dUsuario.eliminarUsuario(cedula);
    }
    
    public LinkedList<Usuario> mostrarInquilino() {
        return this.dUsuario.mostrarInquilino();
    }
    
}
