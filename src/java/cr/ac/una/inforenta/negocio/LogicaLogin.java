
package cr.ac.una.inforenta.negocio;

import cr.ac.una.inforenta.datos.DataLogin;
import cr.ac.una.inforenta.dominio.Login;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogicaLogin {

    private static DataLogin dLogin;

    public LogicaLogin() {
        this.dLogin = new DataLogin();
    }
    
    public boolean insertar(Login login) throws SQLException, ClassNotFoundException{
        return dLogin.insertar(login);
    }
    
    public Login getLogin(int cedula) {
        return this.dLogin.getLogin(cedula);
    }
    
    public boolean eliminarLogin(int cedula) throws SQLException, ClassNotFoundException {
        return dLogin.eliminarLogin(cedula);
    }
    
    public boolean modificarLogin(Login login) {
        boolean modifico=false;
        try {
            modifico = dLogin.modificarContrasena(login);
        } catch (SQLException ex) {
            Logger.getLogger(LogicaLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LogicaLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modifico;
    }
    
    public boolean  iniciarSecion(int cedula, String contrasena) throws SQLException{
        boolean userValido = false; 
        Login user = dLogin.iniciarSecion(cedula, contrasena);
         
         if(user != null){
             if(contrasena.equals(user.getContrasena())){
                 userValido = true;
             }
         }
        return userValido;
    }
    
    public String[] recuperarContraseña(int cedula){
        return dLogin.recuperarContraseña(cedula);
    }
     
}
