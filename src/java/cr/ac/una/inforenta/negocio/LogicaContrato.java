
package cr.ac.una.inforenta.negocio;

import cr.ac.una.inforenta.datos.DataContrato;
import cr.ac.una.inforenta.dominio.Contrato;
import java.sql.SQLException;
import java.util.LinkedList;

public class LogicaContrato {
    
    
    private static DataContrato dContrato;

    public LogicaContrato() {
        this.dContrato= new DataContrato();
    }
   
    public boolean insertarContratos(Contrato contrato) throws SQLException, ClassNotFoundException{
        return dContrato.insertarContratos(contrato);
    }
     public int ultimoIdContrato() throws SQLException, ClassNotFoundException{
        return dContrato.ultimoIdContrato();
    }
     
    public LinkedList<Contrato> getContratos(int cedula) {
        return this.dContrato.getContratos(cedula);
    }
    public boolean  eliminarContrato(int idContrato) throws SQLException, ClassNotFoundException {
        return dContrato.eliminarContrato(idContrato);
    }
    public boolean modificarContrato(Contrato contrato) throws SQLException, ClassNotFoundException{
        return dContrato.modificarContrato(contrato);
    }
    
}
