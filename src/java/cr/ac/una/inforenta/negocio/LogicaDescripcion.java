
package cr.ac.una.inforenta.negocio;

import cr.ac.una.inforenta.datos.DataDescripcion;
import cr.ac.una.inforenta.dominio.Descripcion;
import java.sql.SQLException;
import java.util.LinkedList;

public class LogicaDescripcion {
    
    private static DataDescripcion dDescripcion;

    public LogicaDescripcion() {
        dDescripcion = new DataDescripcion();
    }
    
    public boolean insertarDescripcion(Descripcion descripcion) throws SQLException, ClassNotFoundException{
        return dDescripcion.insertarDescripcionVivienda(descripcion);
    }
    
    
    public LinkedList<Descripcion> getDescripcion(int idVivienda){
        return dDescripcion.getDescripcion(idVivienda);
    }
    
    public boolean  eliminarDescripcion(int idVivienda) throws SQLException, ClassNotFoundException {
        return dDescripcion.eliminarDescripcion(idVivienda);
    }
    
    public boolean actualizarDescripcion(Descripcion descripcion) throws SQLException, ClassNotFoundException{
        return dDescripcion.actualizarDescripcion(descripcion);
    }
    
    public int ultimoId() throws SQLException, ClassNotFoundException{
        return dDescripcion.ultimoIdDescripcion();
    }

}
