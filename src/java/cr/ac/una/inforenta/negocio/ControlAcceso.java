/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cr.ac.una.inforenta.negocio;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class ControlAcceso {
    private final HttpSession session;
    HttpServletResponse response;
    private final String claveSesion = "usuario";
    private final String rutaDeRedireccion = "Login.jsp?err=session";
    
    /*
    Metodo que crea un Control de Sesion con la Sesion actual
    Atributos: HttpSession, HttpServletResponse
    Return: none
    */
    public ControlAcceso(HttpSession session, HttpServletResponse response){
        this.session=session;
        this.response=response;
    }
    
    /*
    Metodo que verifica si la sesion existe
    Atributos: none
    Return none
    */
    public void validarAcceso(){
        
        if(session.getAttribute(claveSesion) == null){
            try {
                response.sendRedirect (rutaDeRedireccion);
            } catch (IOException ex) {
                Logger.getLogger(ControlAcceso.class.getName()).log(Level.SEVERE, null, ex);
            }
       
        }
     
    }
   
}
